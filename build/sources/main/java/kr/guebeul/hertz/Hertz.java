package kr.guebeul.hertz;

import kr.guebeul.hertz.commands.FatigueCommand;
import kr.guebeul.hertz.commands.TempCommand;
import kr.guebeul.hertz.commands.ThirstCommand;
import kr.guebeul.hertz.discord.RPCClient;
import kr.guebeul.hertz.gui.GuiHandler;
import kr.guebeul.hertz.packet.OpenInvPacket;
import kr.guebeul.hertz.packet.OpenRadioPacket;
import kr.guebeul.hertz.packet.SyncInventoryPacket;
import kr.guebeul.hertz.packet.SyncPlayerDataPacket;
import kr.guebeul.hertz.packet.temp.Temp20Packet;
import kr.guebeul.hertz.packet.temp.Temp50Packet;
import kr.guebeul.hertz.packet.temp.Temp70Packet;
import kr.guebeul.hertz.proxy.CommonProxy;
import kr.guebeul.hertz.util.Reference;
import kr.guebeul.hertz.util.handler.EventHandler;
import kr.guebeul.hertz.util.handler.NetworkHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import org.apache.logging.log4j.Logger;

@Mod(modid = Reference.MOD_ID)
public class Hertz {

    public static Logger logger;
    public SimpleNetworkWrapper network;
    public RPCClient rpcClient;

    @SidedProxy(clientSide = Reference.CLIENT_PROXY, serverSide = Reference.COMMON_PROXY)
    public static CommonProxy proxy;

    @Mod.Instance
    public static Hertz instance;

    @Mod.EventHandler
    public  void onPreInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        System.out.println("[ " + Reference.MOD_ID + " ]" + " On PreInit");
        proxy.registerHandlers();
        NetworkHandler.init();
        MinecraftForge.EVENT_BUS.register(new EventHandler());
    }

    @Mod.EventHandler
    public void onInit(FMLInitializationEvent event) {
        System.out.println("[ " + Reference.MOD_ID + " ]" + " On Init");
        instance = this;
        this.rpcClient = new RPCClient("503203468501188608");
        proxy.rpcinit(this);
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
    }

    @Mod.EventHandler
    public void serverInit(FMLServerStartingEvent event) {
        event.registerServerCommand(new FatigueCommand());
        event.registerServerCommand(new TempCommand());
        event.registerServerCommand(new ThirstCommand());
    }

    @Mod.EventHandler
    public void onPostInit(FMLPostInitializationEvent event) {
        proxy.rpcupdate(this, "준비중..");
    }
}
