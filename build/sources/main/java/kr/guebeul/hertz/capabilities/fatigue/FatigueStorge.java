package kr.guebeul.hertz.capabilities.fatigue;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTPrimitive;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagFloat;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class FatigueStorge implements Capability.IStorage<IFatigue> {

    private boolean fagigue = true;

    @Override
    public NBTBase writeNBT(Capability<IFatigue> capability, IFatigue instance, EnumFacing side) {
        if(fagigue = true)
            return new NBTTagFloat(instance.getFatigue());
        else
            return new NBTTagFloat(instance.getMaxFatigue());
    }

    @Override
    public void readNBT(Capability<IFatigue> capability, IFatigue instance, EnumFacing side, NBTBase nbt) {
        instance.set(((NBTPrimitive) nbt).getFloat());
        instance.setTarget(((NBTPrimitive) nbt).getFloat());
    }
}
