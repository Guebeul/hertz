package kr.guebeul.hertz.capabilities.fatigue;

public interface IFatigue {

    public void consume(float points);
    public void fill(float points);
    public void set(float points);

    public void consumeTarget(float points);
    public void fillTarget(float points);
    public void setTarget(float points);

    public float getFatigue();
    public float getMaxFatigue();
    public float getMinFatigue();
    public float getTargetFatigue();

}
