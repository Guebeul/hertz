package kr.guebeul.hertz.capabilities.temperature;

public interface ITemperature {

    public void consume(float points);
    public void fill(float points);
    public void set(float points);

    public void consumeTarget(float points);
    public void fillTarget(float points);
    public void setTarget(float points);

    public float getTemp();
    public float getMaxTemp();
    public float getMinTemp();
    public float getTargetTemp();
}
