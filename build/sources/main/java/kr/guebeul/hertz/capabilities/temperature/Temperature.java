package kr.guebeul.hertz.capabilities.temperature;

public class Temperature implements ITemperature{

    private float temp = 100.0f;
    private float max_temp = 100.0f;
    private float min_temp = 0.0f;
    private float target_temp = 100.0f;

    @Override
    public void consume(float points) {
        this.temp -= points;

        if (temp > max_temp){
            temp = max_temp;
        }
        else if (temp < min_temp) {
            temp = min_temp;
        }
    }

    @Override
    public void fill(float points) {
        this.temp += points;

        if (temp > max_temp){
            temp = max_temp;
        }
        else if (temp < min_temp) {
            temp = min_temp;
        }
    }

    @Override
    public void set(float points) {
        this.temp = points;

        if (temp > max_temp){
            temp = max_temp;
        }
        else if (temp < min_temp) {
            temp = min_temp;
        }
    }

    @Override
    public void consumeTarget(float points) {
        this.temp -= points;
    }

    @Override
    public void fillTarget(float points) {
        this.temp += points;
    }

    @Override
    public void setTarget(float points) {
        this.temp = points;
    }

    @Override
    public float getTemp() {
        return this.temp;
    }

    @Override
    public float getMaxTemp() {
        return this.max_temp;
    }

    @Override
    public float getMinTemp() {
        return this.min_temp;
    }

    @Override
    public float getTargetTemp() {
        return this.target_temp;
    }
}
