package kr.guebeul.hertz.capabilities.thirst;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ThirstProvider implements ICapabilitySerializable<NBTBase> {

    @CapabilityInject(IThirst.class)
    public static final Capability<IThirst> THIRST_CAPABILITY = null;

    private IThirst instance = THIRST_CAPABILITY.getDefaultInstance();

    @Override
    public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == THIRST_CAPABILITY;
    }

    @Nullable
    @Override
    public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing) {
        return capability == THIRST_CAPABILITY ? THIRST_CAPABILITY.<T> cast(this.instance) : null;
    }

    @Override
    public NBTBase serializeNBT() {
        return THIRST_CAPABILITY.getStorage().writeNBT(THIRST_CAPABILITY, this.instance, null);
    }

    @Override
    public void deserializeNBT(NBTBase nbt) {
        THIRST_CAPABILITY.getStorage().readNBT(THIRST_CAPABILITY, this.instance, null, nbt);
    }
}
