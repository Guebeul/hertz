package kr.guebeul.hertz.capabilities.thirst;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTPrimitive;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagFloat;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;

public class ThirstStorage implements Capability.IStorage<IThirst> {

    private boolean thirst = true;

    @Override
    public NBTBase writeNBT(Capability<IThirst> capability, IThirst instance, EnumFacing side) {
        if(thirst = true)
            return new NBTTagFloat(instance.getThirst());
        else
            return new NBTTagFloat(instance.getMaxThirst());
    }

    @Override
    public void readNBT(Capability<IThirst> capability, IThirst instance, EnumFacing side, NBTBase nbt) {
        instance.set(((NBTPrimitive) nbt).getFloat());
        instance.setTarget(((NBTPrimitive) nbt).getFloat());
    }
}
