package kr.guebeul.hertz.commands;

import com.mojang.realmsclient.gui.ChatFormatting;
import kr.guebeul.hertz.capabilities.fatigue.FatigueProvider;
import kr.guebeul.hertz.capabilities.fatigue.IFatigue;
import kr.guebeul.hertz.util.handler.EventHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.command.*;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.event.entity.living.LivingEvent;

public class FatigueCommand extends CommandBase {

    public LivingEvent.LivingUpdateEvent event;

    @Override
    public String getName() {
        return "fatigue";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/fatigue help";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (sender instanceof EntityPlayer) {
            Minecraft mc = Minecraft.getMinecraft();
            EntityPlayer player = (EntityPlayer) event.getEntityLiving();
            IFatigue fatigue = player.getCapability(FatigueProvider.FATIGUE_CAPABILITY, null);
            String title = ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] ";

            if (args.length > 0) {
                if ("fill".equals(args[0])) {
                    int i = parseInt(args[1], 0, 100);
                    fatigue.fill(i);
                    getCommandSenderAsPlayer(sender).sendMessage(new TextComponentString(title + "플레이어의 피로도가 " + ChatFormatting.BOLD + i + ChatFormatting.RESET + "만큼 증가했습니다."));
                }
                if ("consume".equals(args[0])) {
                    int i = parseInt(args[1], 0, 100);
                    fatigue.consume(i);
                    getCommandSenderAsPlayer(sender).sendMessage(new TextComponentString(title + "플레이어의 피로도가 " + ChatFormatting.BOLD + i + ChatFormatting.RESET + "만큼 하락했습니다."));
                }
                if ("set".equals(args[0])) {
                    int i = parseInt(args[1], 0, 100);
                    fatigue.set(i);
                    getCommandSenderAsPlayer(sender).sendMessage(new TextComponentString(title + "플레이어의 피로도가 " + ChatFormatting.BOLD + i + ChatFormatting.RESET + "으로 설정되었습니다."));
                }
                if ("help".equals(args[0])) {
                    getCommandSenderAsPlayer(sender).sendMessage(new TextComponentString(title + ChatFormatting.BOLD + "/fatigue fill <int>" + ChatFormatting.RESET + " : 피로도를 <int> 만큼 증가시킵니다."));
                    getCommandSenderAsPlayer(sender).sendMessage(new TextComponentString(title + ChatFormatting.BOLD + "/fatigue consume <int>" + ChatFormatting.RESET + " : 피로도를 <int> 만큼 하락시킵니다."));
                    getCommandSenderAsPlayer(sender).sendMessage(new TextComponentString(title + ChatFormatting.BOLD + "/fatigue set <int>" + ChatFormatting.RESET + " : 피로도를 <int> 으로 설정합니다."));
                }
            }
        }
    }
}
