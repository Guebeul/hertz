package kr.guebeul.hertz.commands;

import com.mojang.realmsclient.gui.ChatFormatting;
import kr.guebeul.hertz.capabilities.temperature.ITemperature;
import kr.guebeul.hertz.capabilities.temperature.TempProvider;
import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;

public class TempCommand extends CommandBase {

    @Override
    public String getName() {
        return "temp";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/temp help";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (sender instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) sender;
            ITemperature temperature = player.getCapability(TempProvider.TEMP_CAP, null);
            String title = ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] ";

            if (args.length > 0) {
                if ("fill".equals(args[0])) {
                    int i = parseInt(args[1], 0, 100);
                    temperature.fill(i);
                    player.sendMessage(new TextComponentString(title + "플레이어의 체온이 " + ChatFormatting.BOLD + i + ChatFormatting.RESET + "만큼 증가했습니다."));
                }
                if ("consume".equals(args[0])) {
                    int i = parseInt(args[1], 0, 100);
                    temperature.consume(i);
                    player.sendMessage(new TextComponentString(title + "플레이어의 체온이 " + ChatFormatting.BOLD + i + ChatFormatting.RESET + "만큼 하락했습니다."));
                }
                if ("set".equals(args[0])) {
                    int i = parseInt(args[1], 0, 100);
                    temperature.set(i);
                    player.sendMessage(new TextComponentString(title + "플레이어의 체온이 " + ChatFormatting.BOLD + i + ChatFormatting.RESET + "으로 설정되었습니다."));
                }
                if ("help".equals(args[0])) {
                    player.sendMessage(new TextComponentString(title + ChatFormatting.BOLD + "/temp fill <int>" + ChatFormatting.RESET + " : 체온을 <int> 만큼 증가시킵니다."));
                    player.sendMessage(new TextComponentString(title + ChatFormatting.BOLD + "/temp consume <int>" + ChatFormatting.RESET + " : 체온을 <int> 만큼 하락시킵니다."));
                    player.sendMessage(new TextComponentString(title + ChatFormatting.BOLD + "/temp set <int>" + ChatFormatting.RESET + " : 체온을 <int> 으로 설정합니다."));
                }
            }
        }
    }
}
