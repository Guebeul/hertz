package kr.guebeul.hertz.commands;

import com.mojang.realmsclient.gui.ChatFormatting;
import kr.guebeul.hertz.capabilities.thirst.IThirst;
import kr.guebeul.hertz.capabilities.thirst.ThirstProvider;
import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;

public class ThirstCommand extends CommandBase {
    @Override
    public String getName() {
        return "water";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/water help";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (sender instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) sender;
            IThirst thirst = player.getCommandSenderEntity().getCapability(ThirstProvider.THIRST_CAPABILITY, null);
            String title = ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] ";

            if (args.length > 0) {
                if ("fill".equals(args[0])) {
                    int i = parseInt(args[1], 0, 100);
                    thirst.fill(i);
                    player.sendMessage(new TextComponentString(title + "플레이어의 수분이 " + ChatFormatting.BOLD + i + ChatFormatting.RESET + "만큼 증가했습니다."));
                }
                if ("consume".equals(args[0])) {
                    int i = parseInt(args[1], 0, 100);
                    thirst.consume(i);
                    player.sendMessage(new TextComponentString(title + "플레이어의 수분이 " + ChatFormatting.BOLD + i + ChatFormatting.RESET + "만큼 하락했습니다."));
                }
                if ("set".equals(args[0])) {
                    int i = parseInt(args[1], 0, 100);
                    thirst.set(i);
                    player.sendMessage(new TextComponentString(title + "플레이어의 수분이 " + ChatFormatting.BOLD + i + ChatFormatting.RESET + "으로 설정되었습니다."));
                }
                if ("help".equals(args[0])) {
                    player.sendMessage(new TextComponentString(title + ChatFormatting.BOLD + "/water fill <int>" + ChatFormatting.RESET + " : 수분을 <int> 만큼 증가시킵니다."));
                    player.sendMessage(new TextComponentString(title + ChatFormatting.BOLD + "/water consume <int>" + ChatFormatting.RESET + " : 수분을 <int> 만큼 하락시킵니다."));
                    player.sendMessage(new TextComponentString(title + ChatFormatting.BOLD + "/water set <int>" + ChatFormatting.RESET + " : 수분을 <int> 으로 설정합니다."));
                }
            }
        }
    }
}
