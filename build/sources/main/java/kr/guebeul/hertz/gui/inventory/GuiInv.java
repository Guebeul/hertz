package kr.guebeul.hertz.gui.inventory;

import kr.guebeul.hertz.capabilities.fatigue.FatigueProvider;
import kr.guebeul.hertz.capabilities.fatigue.IFatigue;
import kr.guebeul.hertz.capabilities.temperature.ITemperature;
import kr.guebeul.hertz.capabilities.temperature.TempProvider;
import kr.guebeul.hertz.capabilities.thirst.IThirst;
import kr.guebeul.hertz.capabilities.thirst.ThirstProvider;
import kr.guebeul.hertz.util.Reference;
import kr.guebeul.hertz.util.TextureRender;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiInv extends GuiContainer {

    private ResourceLocation INVENTORY_BACKGROUND = new ResourceLocation(Reference.MOD_ID, "textures/gui/inventory.png");
    private ResourceLocation INVENTORY_FATIGUE = new ResourceLocation(Reference.MOD_ID, "textures/gui/inventory/03.png");
    private ResourceLocation INVENTORY_THIRST = new ResourceLocation(Reference.MOD_ID, "textures/gui/inventory/02.png");
    private ResourceLocation INVENTORY_TEMP = new ResourceLocation(Reference.MOD_ID, "textures/gui/inventory/01.png");
    private ResourceLocation INVENTORY_HEALTH = new ResourceLocation(Reference.MOD_ID, "textures/gui/inventory/04.png");
    private ResourceLocation INVENTORY_FOOD = new ResourceLocation(Reference.MOD_ID, "textures/gui/inventory/05.png");

    public static boolean SHOW_DEBUG_NUMS = false;
    private ContainerInv container;
    private CreativeTabs tab;
    final int padding = 6;
    final EntityPlayer thePlayer;

    public GuiInv(EntityPlayer player, InventoryPlayer inventoryPlayer) {
        super(new ContainerInv(player, inventoryPlayer, SHOW_DEBUG_NUMS));
        container = (ContainerInv) this.inventorySlots;
        thePlayer = player;
        this.xSize = Reference.getInvWidth();
        this.ySize = Reference.getInvHeight();
    }

    @Override
    public void initGui() {
        super.initGui();
        int h = 20;
        int w = 20;
        int button_id = 99;
        if (Reference.getMaxSections() > 1) {
            int wid = 50;
            int localStart = this.guiLeft + 70 + 2 * padding;
        }
        w = 6;
        h = 8;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.disableLighting();
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    final int s = 16;

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {

        IFatigue fatigue = mc.player.getCapability(FatigueProvider.FATIGUE_CAPABILITY, null);
        ITemperature temperature = mc.player.getCapability(TempProvider.TEMP_CAP, null);
        IThirst thirst = mc.player.getCapability(ThirstProvider.THIRST_CAPABILITY, null);

        int i = this.guiLeft;
        int j = this.guiTop;
        TextureRender.drawTextureSimple(INVENTORY_BACKGROUND, this.guiLeft, this.guiTop, this.xSize, this.ySize);
        TextureRender.drawTextureSimple(INVENTORY_TEMP, this.guiLeft + 346, this.guiTop + 110, 250, 22);
        TextureRender.drawTextureSimple(INVENTORY_THIRST, this.guiLeft + 346, this.guiTop + 150, 250, 22);
        TextureRender.drawTextureSimple(INVENTORY_FATIGUE, this.guiLeft + 346, this.guiTop + 190, 250, 22);
        TextureRender.drawTextureSimple(INVENTORY_HEALTH, this.guiLeft + 346, this.guiTop + 230, 250, 22);
        TextureRender.drawTextureSimple(INVENTORY_FOOD, this.guiLeft + 346, this.guiTop + 270, 250, 22);

        GlStateManager.pushMatrix(); {
            GlStateManager.scale(1, 1, 1);
            fontRenderer.drawString("+" + temperature.getTemp() + "%", this.guiLeft + 478, this.guiTop + 117, 0xffffff, true);
            fontRenderer.drawString(thirst.getThirst() + "%", this.guiLeft + 486, this.guiTop + 157, 0xffffff, true);
            fontRenderer.drawString(fatigue.getFatigue() + "%", this.guiLeft + 490, this.guiTop + 197, 0xffffff, true);
            fontRenderer.drawString((Math.round(mc.player.getHealth()*10)/10.0) + " / " + mc.player.getMaxHealth(), this.guiLeft + 473, this.guiTop + 237, 0xffffff, true);
            fontRenderer.drawString(mc.player.getFoodStats().getFoodLevel() + " / 20" , this.guiLeft + 480, this.guiTop + 277, 0xffffff, true);
        }
        GlStateManager.popMatrix();

        drawEntityOnScreen(i + 210, j + 280, 100, (float) (i + 210) - mouseX, (float) (j + 280 - 50) - mouseY,
                thePlayer);
    }

    public static void drawEntityOnScreen(int posX, int posY, int scale, float mouseX, float mouseY,
                                          EntityLivingBase ent) {
        GlStateManager.enableColorMaterial();
        GlStateManager.pushMatrix();
        GlStateManager.translate((float) posX, (float) posY, 50.0F);
        GlStateManager.scale((float) (-scale), (float) scale, (float) scale);
        GlStateManager.rotate(180.0F, 0.0F, 0.0F, 1.0F);
        float f = ent.renderYawOffset;
        float f1 = ent.rotationYaw;
        float f2 = ent.rotationPitch;
        float f3 = ent.prevRotationYawHead;
        float f4 = ent.rotationYawHead;
        GlStateManager.rotate(135.0F, 0.0F, 1.0F, 0.0F);
        RenderHelper.enableStandardItemLighting();
        GlStateManager.rotate(-135.0F, 0.0F, 1.0F, 0.0F);
        GlStateManager.rotate(-((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
        ent.renderYawOffset = (float) Math.atan((double) (mouseX / 40.0F)) * 20.0F;
        ent.rotationYaw = (float) Math.atan((double) (mouseX / 40.0F)) * 40.0F;
        ent.rotationPitch = -((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F;
        ent.rotationYawHead = ent.rotationYaw;
        ent.prevRotationYawHead = ent.rotationYaw;
        GlStateManager.translate(0.0F, 0.0F, 0.0F);
        RenderManager rendermanager = Minecraft.getMinecraft().getRenderManager();
        rendermanager.setPlayerViewY(180.0F);
        rendermanager.setRenderShadow(false);
        rendermanager.renderEntity(ent, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F, false);
        rendermanager.setRenderShadow(true);
        ent.renderYawOffset = f;
        ent.rotationYaw = f1;
        ent.rotationPitch = f2;
        ent.prevRotationYawHead = f3;
        ent.rotationYawHead = f4;
        GlStateManager.popMatrix();
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableRescaleNormal();
        GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GlStateManager.disableTexture2D();
        GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
    }
}
