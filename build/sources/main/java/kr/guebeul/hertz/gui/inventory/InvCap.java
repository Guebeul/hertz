package kr.guebeul.hertz.gui.inventory;

import kr.guebeul.hertz.Hertz;
import kr.guebeul.hertz.packet.SyncPlayerDataPacket;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;

import javax.annotation.Nullable;

public class InvCap {

    @CapabilityInject(IPlayerExtendedProperties.class)
    public static final Capability<IPlayerExtendedProperties> CAPABILITYSTORAGE = null;

    public static IPlayerExtendedProperties getPlayerProperties(EntityPlayer player) {
        if (player == null) {
            Hertz.logger.error("Null player, cannot get prop");
            return null;
        }
        IPlayerExtendedProperties props = player.getCapability(CAPABILITYSTORAGE, null);
        return props;
    }

    public interface IPlayerExtendedProperties {
        NBTTagCompound getDataAsNBT();
        void setDataFromNBT(NBTTagCompound nbt);
    }

    public static class InstancePlayerExtendedProperties implements IPlayerExtendedProperties{
        @Override
        public NBTTagCompound getDataAsNBT() {
            NBTTagCompound tags = new NBTTagCompound();
            return tags;
        }

        @Override
        public void setDataFromNBT(NBTTagCompound nbt) {
            NBTTagCompound tags;
            if (nbt instanceof NBTTagCompound == false) {
                tags = new NBTTagCompound();
            } else {
                tags = nbt;
            }
        }
    }

    public static class Storage implements Capability.IStorage<IPlayerExtendedProperties> {
        @Override
        public NBTBase writeNBT(Capability<IPlayerExtendedProperties> capability, IPlayerExtendedProperties instance, EnumFacing side) {
            return instance.getDataAsNBT();
        }

        @Override
        public void readNBT(Capability<IPlayerExtendedProperties> capability, IPlayerExtendedProperties instance, EnumFacing side, NBTBase nbt) {
            try {
                instance.setDataFromNBT((NBTTagCompound)nbt);
            } catch (Exception e) {
                Hertz.logger.error("Invalid NBT Compound ");
                e.printStackTrace();
            }
        }
    }

    public static void syncServerDataToClient(EntityPlayerMP p) {
        if (p == null) { return; }
        IPlayerExtendedProperties props = InvCap.getPlayerProperties(p);
        if (props != null) {
            Hertz.instance.network.sendTo(new SyncPlayerDataPacket(props.getDataAsNBT()), p);
        }
    }
}
