package kr.guebeul.hertz.gui.radio;

import com.mojang.realmsclient.gui.ChatFormatting;
import kr.guebeul.hertz.gui.inventory.GuiInv;
import kr.guebeul.hertz.gui.inventory.inv;
import kr.guebeul.hertz.items.ItemRadio;
import kr.guebeul.hertz.items.ModItems;
import kr.guebeul.hertz.util.Reference;
import kr.guebeul.hertz.util.TextRender;
import kr.guebeul.hertz.util.TextureRender;
import kr.guebeul.hertz.util.handler.SoundsHandler;
import kr.guebeul.hertz.util.timer.HertzTimer;
import kr.guebeul.hertz.util.timer.HertzTimerEvent;
import net.minecraft.client.audio.PositionedSound;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class GuiRadio extends GuiContainer {

    public inv inv;

    private ResourceLocation RADIO_BACKGROUND = new ResourceLocation(Reference.MOD_ID + ":textures/gui/radio/base.png");
    private ResourceLocation RADIO = new ResourceLocation(Reference.MOD_ID + ":textures/gui/radio/radio.png");
    private ContainerRadio container;
    private World world;
    private WorldClient worldClient;
    final EntityPlayer thePlayer;
    public static boolean SHOW_DEBUG_NUMS = false;
    public static boolean no_number_radio = true;
    public static boolean still_radio = false;

    GuiButton button_num0_1;
    GuiButton button_num1_1;
    GuiButton button_num2_1;
    GuiButton button_num3_1;
    GuiButton button_num4_1;
    GuiButton button_num5_1;
    GuiButton button_num6_1;
    GuiButton button_num7_1;
    GuiButton button_num8_1;
    GuiButton button_num9_1;

    final int BUTTON_NUM0_1 = 0;
    final int BUTTON_NUM1_1 = 1;
    final int BUTTON_NUM2_1 = 2;
    final int BUTTON_NUM3_1 = 3;
    final int BUTTON_NUM4_1 = 4;
    final int BUTTON_NUM5_1 = 5;
    final int BUTTON_NUM6_1 = 6;
    final int BUTTON_NUM7_1 = 7;
    final int BUTTON_NUM8_1 = 8;
    final int BUTTON_NUM9_1 = 9;

    String string_num1 = "/";
    String string_num2 = "/";
    String string_num3 = "/";
    String string_num4 = "/";
    String string_num5 = "/";

    public GuiRadio(EntityPlayer player, InventoryPlayer inventoryPlayer) {
        super(new ContainerRadio(player, inventoryPlayer, SHOW_DEBUG_NUMS));
        container = (ContainerRadio) this.inventorySlots;
        thePlayer = player;
        this.xSize = Reference.getInvWidth();
        this.ySize = Reference.getInvHeight();
    }

    @Override
    public void initGui() {
        buttonList.clear();
        //숫자 버튼
        buttonList.add(button_num1_1 = new GuiButton(BUTTON_NUM1_1, this.guiLeft + 480, this.guiTop + 90 + 60, 20, 20, "1"));
        buttonList.add(button_num2_1 = new GuiButton(BUTTON_NUM2_1, this.guiLeft + 510, this.guiTop + 90 + 60, 20, 20, "2"));
        buttonList.add(button_num3_1 = new GuiButton(BUTTON_NUM3_1, this.guiLeft + 540, this.guiTop + 90 + 60, 20, 20, "3"));
        buttonList.add(button_num4_1 = new GuiButton(BUTTON_NUM4_1, this.guiLeft + 480, this.guiTop + 120 + 60, 20, 20, "4"));
        buttonList.add(button_num5_1 = new GuiButton(BUTTON_NUM5_1, this.guiLeft + 510, this.guiTop + 120 + 60, 20, 20, "5"));
        buttonList.add(button_num6_1 = new GuiButton(BUTTON_NUM6_1, this.guiLeft + 540, this.guiTop + 120 + 60, 20, 20, "6"));
        buttonList.add(button_num7_1 = new GuiButton(BUTTON_NUM7_1, this.guiLeft + 480, this.guiTop + 150 + 60, 20, 20, "7"));
        buttonList.add(button_num8_1 = new GuiButton(BUTTON_NUM8_1, this.guiLeft + 510, this.guiTop + 150 + 60, 20, 20, "8"));
        buttonList.add(button_num9_1 = new GuiButton(BUTTON_NUM9_1, this.guiLeft + 540, this.guiTop + 150 + 60, 20, 20, "9"));
        buttonList.add(button_num0_1 = new GuiButton(BUTTON_NUM0_1, this.guiLeft + 510, this.guiTop + 180 + 60, 20, 20, "0"));
        if (no_number_radio == true) {
            button_num6_1.enabled = false;
            button_num7_1.enabled = false;
            button_num8_1.enabled = false;
            button_num9_1.enabled = false;
            button_num0_1.enabled = false;
        }
        super.initGui();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);
    }


    @Override
    protected void actionPerformed(GuiButton button) throws IOException {

        Timer timer = new Timer();

        if (button.id == BUTTON_NUM0_1 & string_num1.equals("/")) {
            string_num1 = "0";
        } else if (button.id == BUTTON_NUM0_1 & string_num2.equals("/")) {
            string_num2 = "0";
        } else if (button.id == BUTTON_NUM0_1 & string_num3.equals("/")) {
            string_num3 = "0";
        } else if (button.id == BUTTON_NUM0_1 & string_num4.equals("/")) {
            string_num4 = "0";
        } else if (button.id == BUTTON_NUM0_1 & string_num5.equals("/")) {
            string_num5 = "0";
        }

        if (button.id == BUTTON_NUM1_1 & string_num1.equals("/")) {
            string_num1 = "1";
        } else if (button.id == BUTTON_NUM1_1 & string_num2.equals("/")) {
            string_num2 = "1";
        } else if (button.id == BUTTON_NUM1_1 & string_num3.equals("/")) {
            string_num3 = "1";
        } else if (button.id == BUTTON_NUM1_1 & string_num4.equals("/")) {
            string_num4 = "1";
        } else if (button.id == BUTTON_NUM1_1 & string_num5.equals("/")) {
            string_num5 = "1";
        }

        if (button.id == BUTTON_NUM2_1 & string_num1.equals("/")) {
            string_num1 = "2";
        } else if (button.id == BUTTON_NUM2_1 & string_num2.equals("/")) {
            string_num2 = "2";
        } else if (button.id == BUTTON_NUM2_1 & string_num3.equals("/")) {
            string_num3 = "2";
        } else if (button.id == BUTTON_NUM2_1 & string_num4.equals("/")) {
            string_num4 = "2";
        } else if (button.id == BUTTON_NUM2_1 & string_num5.equals("/")) {
            string_num5 = "2";
        }

        if (button.id == BUTTON_NUM3_1 & string_num1.equals("/")) {
            string_num1 = "3";
        } else if (button.id == BUTTON_NUM3_1 & string_num2.equals("/")) {
            string_num2 = "3";
        } else if (button.id == BUTTON_NUM3_1 & string_num3.equals("/")) {
            string_num3 = "3";
        } else if (button.id == BUTTON_NUM3_1 & string_num4.equals("/")) {
            string_num4 = "3";
        } else if (button.id == BUTTON_NUM3_1 & string_num5.equals("/")) {
            string_num5 = "3";
        }

        if (button.id == BUTTON_NUM4_1 & string_num1.equals("/")) {
            string_num1 = "4";
        } else if (button.id == BUTTON_NUM4_1 & string_num2.equals("/")) {
            string_num2 = "4";
        } else if (button.id == BUTTON_NUM4_1 & string_num3.equals("/")) {
            string_num3 = "4";
        } else if (button.id == BUTTON_NUM4_1 & string_num4.equals("/")) {
            string_num4 = "4";
        } else if (button.id == BUTTON_NUM4_1 & string_num5.equals("/")) {
            string_num5 = "4";
        }

        if (button.id == BUTTON_NUM5_1 & string_num1.equals("/")) {
            string_num1 = "5";
        } else if (button.id == BUTTON_NUM5_1 & string_num2.equals("/")) {
            string_num2 = "5";
        } else if (button.id == BUTTON_NUM5_1 & string_num3.equals("/")) {
            string_num3 = "5";
        } else if (button.id == BUTTON_NUM5_1 & string_num4.equals("/")) {
            string_num4 = "5";
        } else if (button.id == BUTTON_NUM5_1 & string_num5.equals("/")) {
            string_num5 = "5";
        }

        if (button.id == BUTTON_NUM6_1 & string_num1.equals("/")) {
            string_num1 = "6";
        } else if (button.id == BUTTON_NUM6_1 & string_num2.equals("/")) {
            string_num2 = "6";
        } else if (button.id == BUTTON_NUM6_1 & string_num3.equals("/")) {
            string_num3 = "6";
        } else if (button.id == BUTTON_NUM6_1 & string_num4.equals("/")) {
            string_num4 = "6";
        } else if (button.id == BUTTON_NUM6_1 & string_num5.equals("/")) {
            string_num5 = "6";
        }

        if (button.id == BUTTON_NUM7_1 & string_num1.equals("/")) {
            string_num1 = "7";
        } else if (button.id == BUTTON_NUM7_1 & string_num2.equals("/")) {
            string_num2 = "7";
        } else if (button.id == BUTTON_NUM7_1 & string_num3.equals("/")) {
            string_num3 = "7";
        } else if (button.id == BUTTON_NUM7_1 & string_num4.equals("/")) {
            string_num4 = "7";
        } else if (button.id == BUTTON_NUM7_1 & string_num5.equals("/")) {
            string_num5 = "7";
        }

        if (button.id == BUTTON_NUM8_1 & string_num1.equals("/")) {
            string_num1 = "8";
        } else if (button.id == BUTTON_NUM8_1 & string_num2.equals("/")) {
            string_num2 = "8";
        } else if (button.id == BUTTON_NUM8_1 & string_num3.equals("/")) {
            string_num3 = "8";
        } else if (button.id == BUTTON_NUM8_1 & string_num4.equals("/")) {
            string_num4 = "8";
        } else if (button.id == BUTTON_NUM8_1 & string_num5.equals("/")) {
            string_num5 = "8";
        }

        if (button.id == BUTTON_NUM9_1 & string_num1.equals("/")) {
            string_num1 = "9";
        } else if (button.id == BUTTON_NUM9_1 & string_num2.equals("/")) {
            string_num2 = "9";
        } else if (button.id == BUTTON_NUM9_1 & string_num3.equals("/")) {
            string_num3 = "9";
        } else if (button.id == BUTTON_NUM9_1 & string_num4.equals("/")) {
            string_num4 = "9";
        } else if (button.id == BUTTON_NUM9_1 & string_num5.equals("/")) {
            string_num5 = "9";
        }

        //녹음번호 12221 (1번)
        if (string_num1 == "1" && string_num2 == "2" && string_num3 == "2" && string_num4 == "2" && string_num5 == "1") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 점점 거세지는 눈보라에 많은 국민들이 안전지대로 피난을 떠나고 있습니다. 아직 안전지대로 오지 못하신 분들은 안내방송을 듣고 해당 좌표로 안전히 오시길 바랍니다. " + ChatFormatting.BLUE + "-1245 69 -222"));
            thePlayer.playSound(SoundsHandler.NUMBER_12221, 1.0F,1.0F);
        }

        //녹음번호 24245 (2번)
        if (string_num1 == "2" && string_num2 == "4" && string_num3 == "2" && string_num4 == "4" && string_num5 == "5") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 다음 좌표입니다. " + ChatFormatting.BLUE + "-606 88 -359"));
            thePlayer.playSound(SoundsHandler.NUMBER_24245, 1.0F,1.0F);
        }

        //녹음번호 34221 (3번)
        if (string_num1 == "3" && string_num2 == "4" && string_num3 == "2" && string_num4 == "2" && string_num5 == "1") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 다음 좌표입니다. " + ChatFormatting.BLUE + "-143 70 -1206"));
            thePlayer.playSound(SoundsHandler.NUMBER_34221, 1.0F,1.0F);
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    still_radio = true;
                    thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 라디오가 갑자기 작동하지 않습니다."));
                    thePlayer.inventory.addItemStackToInventory(new ItemStack(Items.NETHER_WART).setStackDisplayName(ChatFormatting.DARK_PURPLE + "라디오"));
                    thePlayer.inventory.clearMatchingItems(ModItems.NoNumRadio, 0, 1, null);
                }
            }, 13*1000);
        }

        //녹음번호 54311 (체크1번)
        if (string_num1 == "5" && string_num2 == "4" && string_num3 == "3" && string_num4 == "1" && string_num5 == "1") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 날이 갈수록 기온이 떨어지고 있습니다. 서둘러 이하 좌표로 이동하시길 바랍니다 " + ChatFormatting.BLUE + "477 67 -1182"));
            thePlayer.playSound(SoundsHandler.NUMBER_54311, 1.0F,1.0F);
        }

        //녹음번호 33112 (4번)
        if (string_num1 == "3" && string_num2 == "3" && string_num3 == "1" && string_num4 == "1" && string_num5 == "2") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 다음 좌표입니다. " + ChatFormatting.BLUE + "1253 75 -860"));
            thePlayer.playSound(SoundsHandler.NUMBER_33112, 1.0F,1.0F);
        }

        //녹음번호 55553 (5번)
        if (string_num1 == "5" && string_num2 == "5" && string_num3 == "5" && string_num4 == "5" && string_num5 == "3") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 다음 좌표입니다. " + ChatFormatting.BLUE + "1549 71 -1068"));
            thePlayer.playSound(SoundsHandler.NUMBER_55553, 1.0F,1.0F);
        }

        //녹음번호 44551 (6번)
        if (string_num1 == "4" && string_num2 == "4" && string_num3 == "5" && string_num4 == "5" && string_num5 == "1") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 다음 좌표입니다. " + ChatFormatting.BLUE + "1796 86 -1562"));
            thePlayer.playSound(SoundsHandler.NUMBER_44551, 1.0F,1.0F);
        }

        //녹음번호 21442 (7번)
        if (string_num1 == "2" && string_num2 == "1" && string_num3 == "4" && string_num4 == "4" && string_num5 == "2") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 다음 좌표입니다. " + ChatFormatting.BLUE + "1256 126 -1406"));
            thePlayer.playSound(SoundsHandler.NUMBER_21442, 1.0F,1.0F);
        }

        //녹음번호 00317 (8번)
        if (string_num1 == "0" && string_num2 == "0" && string_num3 == "3" && string_num4 == "1" && string_num5 == "7") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 다음 좌표입니다. " + ChatFormatting.BLUE + "2578 69 -983"));
            thePlayer.playSound(SoundsHandler.NUMBER_00317, 1.0F,1.0F);
        }

        //녹음번호 88818 (9번)
        if (string_num1 == "8" && string_num2 == "8" && string_num3 == "8" && string_num4 == "1" && string_num5 == "8") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 다음 좌표입니다. " + ChatFormatting.BLUE + "2042 71 -303"));
            thePlayer.playSound(SoundsHandler.NUMBER_88818, 1.0F,1.0F);
        }

        //녹음번호 69830 (10번)
        if (string_num1 == "6" && string_num2 == "9" && string_num3 == "8" && string_num4 == "3" && string_num5 == "0") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 다음 좌표입니다. " + ChatFormatting.BLUE + "1402 73 366"));
            thePlayer.playSound(SoundsHandler.NUMBER_69830, 1.0F,1.0F);
        }
        //녹음번호 18905 (11번)
        if (string_num1 == "1" && string_num2 == "8" && string_num3 == "9" && string_num4 == "0" && string_num5 == "5") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 다음 좌표입니다. " + ChatFormatting.BLUE + "406 86 1800"));
            thePlayer.playSound(SoundsHandler.NUMBER_18905, 1.0F,1.0F);
        }

        //녹음번호 10109 (체크2번)
        if (string_num1 == "1" && string_num2 == "0" && string_num3 == "1" && string_num4 == "0" && string_num5 == "9") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 노후한 건물들이 얼어붙어 무너지는 사고가 지속적으로 일어나고 있습니다. 안전하게 이하 좌표로 이동하시길 바랍니다. " + ChatFormatting.BLUE + "-80 68 1507"));
            thePlayer.playSound(SoundsHandler.NUMBER_10109, 1.0F,1.0F);
        }

        //녹음번호 99645(체크3번)
        if (string_num1 == "9" && string_num2 == "9" && string_num3 == "6" && string_num4 == "4" && string_num5 == "5") {
            mc.displayGuiScreen(null);
            thePlayer.sendMessage(new TextComponentString(ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] 거센 눈보라가 도심 곳곳에 피해를 주고 있습니다. 국민 여러분의 안전을 기원하며 마지막 좌표를 불러드리겠습니다. " + ChatFormatting.BLUE + "-1772 71 1474"));
            thePlayer.playSound(SoundsHandler.NUMBER_99645, 1.0F,1.0F);
        }
        super.actionPerformed(button);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        int i = this.guiLeft;
        int j = this.guiTop;
        TextureRender.drawTextureSimple(RADIO_BACKGROUND, this.guiLeft, this.guiTop, this.xSize, this.ySize);
        TextureRender.drawTextureSimple(RADIO, this.guiLeft + 50 - 30, this.guiTop + 50, 340, 250);
        GlStateManager.pushMatrix(); {
            GlStateManager.scale(2, 2, 2);
            fontRenderer.drawString(string_num1, this.guiLeft + 100 - 30, this.guiTop + 75, 0xffffff, true);
        }
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix(); {
            GlStateManager.scale(2, 2, 2);
            fontRenderer.drawString(string_num2, this.guiLeft + 110 - 30, this.guiTop + 75, 0xffffff, true);
        }
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix(); {
            GlStateManager.scale(2, 2, 2);
            fontRenderer.drawString(string_num3, this.guiLeft + 120 - 30, this.guiTop + 75, 0xffffff, true);
        }
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix(); {
            GlStateManager.scale(2, 2, 2);
            fontRenderer.drawString(string_num4, this.guiLeft + 130 - 30, this.guiTop + 75, 0xffffff, true);
        }
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix(); {
            GlStateManager.scale(2, 2, 2);
            fontRenderer.drawString(string_num5, this.guiLeft + 140 - 30, this.guiTop + 75, 0xffffff, true);
        }
        GlStateManager.popMatrix();
    }
}
