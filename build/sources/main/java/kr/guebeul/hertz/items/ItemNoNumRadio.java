package kr.guebeul.hertz.items;

import kr.guebeul.hertz.Hertz;
import kr.guebeul.hertz.gui.radio.GuiRadio;
import kr.guebeul.hertz.packet.OpenRadioPacket;
import kr.guebeul.hertz.util.handler.NetworkHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemNoNumRadio extends Item {


    public ItemNoNumRadio(String name) {
        setUnlocalizedName(name);
        setRegistryName(name);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        if (!worldIn.isRemote) {
            System.out.println("번호판 없는 라디오 오픈");
        } else {
            NetworkHandler.sendToServer(new OpenRadioPacket());
            GuiRadio.no_number_radio = true;
        }
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }
}
