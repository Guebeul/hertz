package kr.guebeul.hertz.items;

import kr.guebeul.hertz.util.Reference;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = Reference.MOD_ID)
public class ModItems {

    public static Item radio;
    public static Item NoNumRadio;

    public static void init() {
        radio = new ItemRadio("radio").setCreativeTab(tabHertz).setMaxStackSize(1);
        NoNumRadio = new ItemNoNumRadio("no_num_radio").setCreativeTab(tabHertz).setMaxStackSize(1);
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(radio);
        event.getRegistry().registerAll(NoNumRadio);
    }

    @SubscribeEvent
    public static void registerRenders(ModelRegistryEvent event) {
        registerRender(radio);
        registerRender(NoNumRadio);
    }

    private static void registerRender(Item item) {
        ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory"));
    }

    static final CreativeTabs tabHertz = new CreativeTabs("hertz") {
        @Override
        public ItemStack getTabIconItem() {
            return new ItemStack(radio);
        }

        @Override
        public boolean hasSearchBar() {
            return true;
        }
    }.setBackgroundImageName("item_search.png");
}
