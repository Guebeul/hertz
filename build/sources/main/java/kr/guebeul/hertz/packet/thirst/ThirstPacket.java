package kr.guebeul.hertz.packet.thirst;

import com.mojang.realmsclient.gui.ChatFormatting;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ThirstPacket implements IMessage, IMessageHandler<ThirstPacket, IMessage> {

    @Override
    public void toBytes(ByteBuf buf) {}

    @Override
    public void fromBytes(ByteBuf buf) {}

    @Override
    public IMessage onMessage(ThirstPacket message, MessageContext ctx) {
        String title = "[ " + ChatFormatting.DARK_PURPLE + "헤르츠" + ChatFormatting.WHITE + " ] ";
        EntityPlayer p = ctx.getServerHandler().player;
        p.sendMessage(new TextComponentString(title + "수분이 부족해 갈증이 심해집니다!"));
        p.addPotionEffect(new PotionEffect(MobEffects.MINING_FATIGUE, 99999, 1));
        return null;
    }

    public static class ThirstPacketExit implements IMessage, IMessageHandler<ThirstPacketExit, IMessage> {
        @Override
        public void toBytes(ByteBuf buf) {}

        @Override
        public void fromBytes(ByteBuf buf) {}

        @Override
        public IMessage onMessage(ThirstPacketExit message, MessageContext ctx) {
            String title = "[ " + ChatFormatting.DARK_PURPLE + "헤르츠" + ChatFormatting.WHITE + " ] ";
            EntityPlayer p = ctx.getServerHandler().player;
            p.sendMessage(new TextComponentString(title + "갈증이 조금 가신 것 같습니다."));
            p.removePotionEffect(MobEffects.MINING_FATIGUE);
            return null;
        }
    }
}
