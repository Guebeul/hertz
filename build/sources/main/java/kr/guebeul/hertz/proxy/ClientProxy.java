package kr.guebeul.hertz.proxy;

import kr.guebeul.hertz.Hertz;
import kr.guebeul.hertz.gui.HertzHUD;
import kr.guebeul.hertz.gui.inventory.InvCap;
import kr.guebeul.hertz.util.Reference;
import kr.guebeul.hertz.util.handler.EventHandler;
import kr.guebeul.hertz.util.handler.InvEventHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IThreadListener;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;

@SideOnly(Side.CLIENT)
public class ClientProxy extends CommonProxy {
    public static KeyBinding keyInv;
    public static final String keyCategory = "key.categories.hz";
    private ResourceLocation bearModel = new ResourceLocation(Reference.MOD_ID,"models/bear/bear.obm");
    private ResourceLocation bearTexture = new ResourceLocation(Reference.MOD_ID,"models/bear/texture.png");

    @Override
    public void registerHandlers() {
        super.registerHandlers();
        keyInv = new KeyBinding("key.openinventory", Keyboard.KEY_B, keyCategory);
        CapabilityManager.INSTANCE.register(InvCap.IPlayerExtendedProperties.class, new InvCap.Storage(), InvCap.InstancePlayerExtendedProperties::new);
        ClientRegistry.registerKeyBinding(ClientProxy.keyInv);
        MinecraftForge.EVENT_BUS.register(new InvEventHandler());
        MinecraftForge.EVENT_BUS.register(new HertzHUD());
    }

    @Override
    public IThreadListener getThreadFromContext(MessageContext ctx) {
        return (ctx.side.isClient() ? Minecraft.getMinecraft() : super.getThreadFromContext(ctx));
    }

    @Override
    public void setClientPlayerData(MessageContext ctx, NBTTagCompound tags) {
        EntityPlayer p = Minecraft.getMinecraft().player;
        if (p != null) {
            InvCap.IPlayerExtendedProperties props = InvCap.getPlayerProperties(Minecraft.getMinecraft().player);
            if (props != null) {
                props.setDataFromNBT(tags);
            }
        }
    }

    @Override
    public void rpcinit(Hertz main) {
        main.rpcClient.init();
    }

    @Override
    public void rpcupdate(Hertz main, String details) {
        main.rpcClient.upddatePresence(details);
    }
}
