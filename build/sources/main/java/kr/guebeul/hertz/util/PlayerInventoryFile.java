package kr.guebeul.hertz.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import com.google.common.io.Files;
import kr.guebeul.hertz.Hertz;
import kr.guebeul.hertz.gui.inventory.inv;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PlayerInventoryFile {
    public static final String EXT = "헤르츠";
    public static final String EXTBK = "헤르츠2";
    public static HashSet<Integer> playerEntityIds = new HashSet<Integer>();
    private static HashMap<String, inv> playerItems = new HashMap<String, inv>();
    public static void playerSetupOnLoad(PlayerEvent.LoadFromFile event) {
        EntityPlayer player = event.getEntityPlayer();
        clearPlayerInventory(player);
        File playerFile = getPlayerFile(EXT, event.getPlayerDirectory(), event.getEntityPlayer());
        if (!playerFile.exists()) {
            File fileNew = event.getPlayerFile(EXT);
            if (fileNew.exists()) {
                try {
                    Files.copy(fileNew, playerFile);
                    Hertz.logger.info("Using and converting UUID savefile for " + player.getDisplayNameString());
                    fileNew.delete();
                    File fb = event.getPlayerFile(EXTBK);
                    if (fb.exists())
                        fb.delete();
                }
                catch (IOException e) {
                }
            }
        }
        loadPlayerInventory(event.getEntityPlayer(), playerFile, getPlayerFile(EXTBK, event.getPlayerDirectory(), event.getEntityPlayer()));
        playerEntityIds.add(event.getEntityPlayer().getEntityId());
    }
    public static void clearPlayerInventory(EntityPlayer player) {
        playerItems.remove(player.getDisplayNameString());
    }
    public static inv getPlayerInventory(EntityPlayer player) {
        if (!playerItems.containsKey(player.getDisplayNameString())) {
            inv inventory = new inv(player);
            playerItems.put(player.getDisplayNameString(), inventory);
        }
        return playerItems.get(player.getDisplayNameString());
    }
    public static ItemStack getPlayerInventoryStack(EntityPlayer player, int slot) {
        return getPlayerInventory(player).getStackInSlot(slot);
    }
    public static void setPlayerInventoryStack(EntityPlayer player, int slot, ItemStack itemStack) {
        getPlayerInventory(player).inventory.set(slot, itemStack);
    }
    public static void setPlayerInventory(EntityPlayer player, inv inventory) {
        playerItems.put(player.getDisplayNameString(), inventory);
    }
    public static void loadPlayerInventory(EntityPlayer player, File file1, File file2) {
        if (player != null && !player.getEntityWorld().isRemote) {
            try {
                NBTTagCompound data = null;
                boolean save = false;
                if (file1 != null && file1.exists()) {
                    try {
                        FileInputStream fileinputstream = new FileInputStream(file1);
                        data = CompressedStreamTools.readCompressed(fileinputstream);
                        fileinputstream.close();
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (file1 == null || !file1.exists() || data == null || data.hasNoTags()) {
                    Hertz.logger.warn("Data not found for " + player.getDisplayNameString() + ". Trying to load backup data.");
                    if (file2 != null && file2.exists()) {
                        try {
                            FileInputStream fileinputstream = new FileInputStream(file2);
                            data = CompressedStreamTools.readCompressed(fileinputstream);
                            fileinputstream.close();
                            save = true;
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (data != null) {
                    inv inventory = new inv(player);
                    inventory.readFromNBT(data);
                    playerItems.put(player.getDisplayNameString(), inventory);
                    if (save)
                        savePlayerItems(player, file1, file2);
                }
            }
            catch (Exception e) {
                Hertz.logger.error("Error loading player extended inventory");
                e.printStackTrace();
            }
        }
    }
    public static void savePlayerItems(EntityPlayer player, File file1, File file2) {
        if (player != null && !player.getEntityWorld().isRemote) {
            try {
                if (file1 != null && file1.exists()) {
                    try {
                        Files.copy(file1, file2);
                    }
                    catch (Exception e) {
                        Hertz.logger.error("Could not backup old file for player " + player.getDisplayNameString());
                    }
                }
                try {
                    if (file1 != null) {
                        inv inventory = getPlayerInventory(player);
                        NBTTagCompound data = new NBTTagCompound();
                        inventory.writeToNBT(data);
                        FileOutputStream fileoutputstream = new FileOutputStream(file1);
                        CompressedStreamTools.writeCompressed(data, fileoutputstream);
                        fileoutputstream.close();
                    }
                }
                catch (Exception e) {
                    Hertz.logger.error("Could not save file for player " + player.getDisplayNameString());
                    e.printStackTrace();
                    if (file1.exists()) {
                        try {
                            file1.delete();
                        }
                        catch (Exception e2) {
                        }
                    }
                }
            }
            catch (Exception exception1) {
                Hertz.logger.error("Error saving inventory");
                exception1.printStackTrace();
            }
        }
    }
    public static File getPlayerFile(String suffix, File playerDirectory, EntityPlayer player) {
        String playername = player.getUniqueID().toString();
        String file =  "_" + playername + "." + suffix;
        Hertz.logger.info("Player File: "+file);
        return new File(playerDirectory, file);
    }

    public static void syncItems(EntityPlayer player) {
        inv invo = getPlayerInventory(player);
        for (int a = 0; a < invo.getSizeInventory(); a++) {
            getPlayerInventory(player).syncSlotToClient(a);
        }
    }
    public static void putDataIntoInventory(inv invo, EntityPlayer player) {
        inv fromStorage = getPlayerInventory(player);
        invo.inventory = fromStorage.inventory;
    }
}
