package kr.guebeul.hertz.util.handler;

import com.mojang.realmsclient.gui.ChatFormatting;
import kr.guebeul.hertz.Hertz;
import kr.guebeul.hertz.capabilities.fatigue.FatigueProvider;
import kr.guebeul.hertz.capabilities.fatigue.IFatigue;
import kr.guebeul.hertz.capabilities.temperature.ITemperature;
import kr.guebeul.hertz.capabilities.temperature.TempProvider;
import kr.guebeul.hertz.capabilities.thirst.IThirst;
import kr.guebeul.hertz.capabilities.thirst.Thirst;
import kr.guebeul.hertz.capabilities.thirst.ThirstProvider;
import kr.guebeul.hertz.gui.inventory.GuiInv;
import kr.guebeul.hertz.packet.OpenInvPacket;
import kr.guebeul.hertz.packet.OpenRadioPacket;
import kr.guebeul.hertz.packet.fatigue.FatiguePacket;
import kr.guebeul.hertz.packet.food.FoodPacket;
import kr.guebeul.hertz.packet.temp.Temp20Packet;
import kr.guebeul.hertz.packet.temp.Temp50Packet;
import kr.guebeul.hertz.packet.temp.Temp70Packet;
import kr.guebeul.hertz.packet.thirst.ThirstPacket;
import kr.guebeul.hertz.proxy.ClientProxy;
import kr.guebeul.hertz.util.NearBlock;
import kr.guebeul.hertz.util.PlayerInventoryFile;
import net.minecraft.advancements.critereon.EntityHurtPlayerTrigger;
import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandHandler;
import net.minecraft.command.ICommandManager;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.MobEffects;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingDestroyBlockEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Iterator;

@Mod.EventBusSubscriber
public class EventHandler {

    private boolean temp70 = false;
    private boolean temp50 = false;
    private boolean temp20 = false;
    private boolean thirst10 = false;
    private boolean food4 = false;
    private boolean fatigue80 = false;

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void onKeyInput(InputEvent.KeyInputEvent event) {
        Minecraft mc = Minecraft.getMinecraft();
        if (ClientProxy.keyInv.isPressed()) {
            NetworkHandler.sendToServer(new OpenInvPacket());
        }
    }

    @SubscribeEvent
    public void BlockBreak(LivingDestroyBlockEvent event) {
        if (event.getEntityLiving() instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) event.getEntityLiving();
            IFatigue fatigue = player.getCapability(FatigueProvider.FATIGUE_CAPABILITY, null);
            fatigue.fill(1.0f);
        }
    }

    @SubscribeEvent
    public void clone(PlayerEvent.Clone event) {
        if (event.getEntity() instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) event.getEntity();
            IFatigue fatigue = player.getCapability(FatigueProvider.FATIGUE_CAPABILITY, null);
            ITemperature temperature = player.getCapability(TempProvider.TEMP_CAP, null);
            IThirst thirst = player.getCapability(ThirstProvider.THIRST_CAPABILITY, null);
            fatigue.set(0.0f);
            temperature.set(100.0f);
            thirst.set(100.0f);
        }
    }
    @SubscribeEvent
    public void livingJoin(net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent event) {
        if (event.player instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer)event.player;
            MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
            ICommandManager commandManager = server.commandManager;
            player.clearActivePotions();
            IFatigue fatigue = player.getCapability(FatigueProvider.FATIGUE_CAPABILITY, null);
            ITemperature temperature = player.getCapability(TempProvider.TEMP_CAP, null);
            IThirst thirst = player.getCapability(ThirstProvider.THIRST_CAPABILITY, null);
            fatigue.set(0.0f);
            temperature.set(100.0f);
            thirst.set(100.0f);
        }
    }

    @SubscribeEvent
    public void livingDeath(LivingDeathEvent event) {
        if (event.getEntityLiving() instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer)event.getEntityLiving();
            IFatigue fatigue = player.getCapability(FatigueProvider.FATIGUE_CAPABILITY, null);
            ITemperature temperature = player.getCapability(TempProvider.TEMP_CAP, null);
            IThirst thirst = player.getCapability(ThirstProvider.THIRST_CAPABILITY, null);
            fatigue.set(0.0f);
            temperature.set(100.0f);
            thirst.set(100.0f);
        }
    }

    @SubscribeEvent
    public void livingRespawn(net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent event) {
        if (event.player instanceof EntityPlayer) {
            IFatigue fatigue = event.player.getCapability(FatigueProvider.FATIGUE_CAPABILITY, null);
            ITemperature temperature = event.player.getCapability(TempProvider.TEMP_CAP, null);
            IThirst thirst = event.player.getCapability(ThirstProvider.THIRST_CAPABILITY, null);
            fatigue.set(0.0f);
            temperature.set(100.0f);
            thirst.set(100.0f);
            event.player.setHealth(event.player.getMaxHealth());
        }
    }

    @SubscribeEvent
    public void playerevent(LivingEvent event) {
        if (event.getEntityLiving() instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) event.getEntityLiving();
            IFatigue fatigue = player.getCapability(FatigueProvider.FATIGUE_CAPABILITY, null);
            ITemperature temperature = player.getCapability(TempProvider.TEMP_CAP, null);
            IThirst thirst = player.getCapability(ThirstProvider.THIRST_CAPABILITY, null);

            if (temperature.getTemp() < 70.0F && temperature.getTemp() > 50.0F) {
                if (!temp70) {
                    NetworkHandler.sendToServer(new Temp70Packet());
                    temp70 = true;
                }
                if (player.ticksExisted % 200 == 0 && !player.isCreative()) { }
            }

            if (temperature.getTemp() < 50.0F && temperature.getTemp() > 20.0F) {
                if (!temp50) {
                    NetworkHandler.sendToServer(new Temp50Packet());
                    temp50 = true;
                }
            }

            if (temperature.getTemp() < 20.0F && temperature.getTemp() > 0.0F) {
                if (!temp20) {
                    NetworkHandler.sendToServer(new Temp20Packet());
                    temp20 = true;
                }
            }

            if (thirst.getThirst() < 10F && thirst.getThirst() > 0.0F) {
                if (!thirst10) {
                    NetworkHandler.sendToServer(new ThirstPacket());
                    thirst10 = true;
                }
                if (player.ticksExisted % 100 == 0) {}
            }
            if (thirst.getThirst() > 10F && thirst10) {
                NetworkHandler.sendToServer(new ThirstPacket.ThirstPacketExit());
                thirst10 = false;
            }

            if (player.getFoodStats().getFoodLevel() < 5.0F && thirst.getThirst() > 0.0F) {
                if (!food4) {
                    NetworkHandler.sendToServer(new FoodPacket());
                    food4 = true;
                }
                if (player.ticksExisted % 200 == 0 && !player.isCreative()) {}
            }
            if (player.getFoodStats().getFoodLevel() > 5.0F) {
                if (food4) {
                    NetworkHandler.sendToServer(new FoodPacket.FoodPacketExit());
                    food4 = false;
                }
            }

            if (fatigue.getFatigue() > 80F && fatigue.getFatigue() < 100F) {
                if (!fatigue80) {
                    NetworkHandler.sendToServer(new FatiguePacket());
                    fatigue80 = true;
                }
                if (player.ticksExisted % 100 == 0) { }
            }
            if (fatigue.getFatigue() < 80F && fatigue80) {
                NetworkHandler.sendToServer(new FatiguePacket.FatiguePacketExit());
                fatigue80 = false;
            }
        }
    }

    @SubscribeEvent
    public static void onLivingUpdate(LivingEvent.LivingUpdateEvent event) {
        if (event.getEntityLiving() instanceof EntityPlayer) {
            String title = ChatFormatting.WHITE + "[ " + ChatFormatting.DARK_PURPLE + "헤르츠 " + ChatFormatting.WHITE + "] ";
            EntityPlayer player = (EntityPlayer) event.getEntityLiving();
            IFatigue fatigue = player.getCapability(FatigueProvider.FATIGUE_CAPABILITY, null);
            ITemperature temperature = player.getCapability(TempProvider.TEMP_CAP, null);
            IThirst thirst = player.getCapability(ThirstProvider.THIRST_CAPABILITY, null);
            BlockPos blockPos = player.getPosition();
            // ================================
            //            피로도
            // ================================
            //피로도가 100 이랑 같거나 이상일 경우
            if (fatigue.getFatigue() >= fatigue.getMaxFatigue()) {
                fatigue.set(fatigue.getMaxFatigue());
            }

            //피로도가 0 이하일 경우
            if (fatigue.getFatigue() < 0.0f) {
                fatigue.set(0.0f);
            }

            //움직임 피로도
            if (player.ticksExisted % 60 == 0 && player.limbSwingAmount > 0.02F) {
                fatigue.fill(1.0f);
            } else if (player.ticksExisted % 60 == 0 && player.limbSwingAmount < 0.02F) {
                fatigue.consume(1.0f);
            }


            // ================================
            //            체온
            // ================================
            //체온이 100이랑 같거나 이상일 경우
            if (temperature.getTemp() >= temperature.getMaxTemp()) {
                temperature.set(temperature.getMaxTemp());
            }

            //체온이 0 이하 일경우
            if (temperature.getTemp() < 0.0f) {
                temperature.set(0.0f);
            }

            //플레이어가 빛을 볼 경우
            if (player.ticksExisted % 200 == 0 && player.world.isDaytime() && player.world.canBlockSeeSky(blockPos)) {
                temperature.consume(1.0f);
            } else if (player.ticksExisted % 200 == 0 && !player.world.canBlockSeeSky(blockPos)) {
                temperature.fill(1.0f);
            }

            //물 안에 있을 시
            if (player.ticksExisted % 100 == 0 && player.isInWater()) {
                temperature.consume(2.0f);
            }

            //불 근처에 있을 시
            if (NearBlock.isBlockNextToPlayer(blockPos.getX(), blockPos.getY(), blockPos.getZ(), Blocks.FIRE, player)) {
                if (player.ticksExisted % 100 == 0 && player.world.canBlockSeeSky(blockPos)) {
                    temperature.fill(2.0f);
                }
            } else if (NearBlock.isBlockNearPlayer2(blockPos.getX(), blockPos.getY(), blockPos.getZ(), Blocks.FIRE, player, false)) {
                if (player.ticksExisted % 100 == 0 && player.world.canBlockSeeSky(blockPos)) {
                    temperature.fill(2.0f);
                }
            }

            //갑옷
            Iterator<ItemStack> armorList = player.getArmorInventoryList().iterator();
            while (armorList.hasNext()) {
                ItemStack element = armorList.next();

                //사슬갑옷 세트
                if (player.ticksExisted % 450 == 0 && ItemStack.areItemsEqualIgnoreDurability(element, new ItemStack(Items.CHAINMAIL_BOOTS))) {
                    temperature.fill(0.5f);
                } else if (player.ticksExisted % 450 == 0 && ItemStack.areItemsEqualIgnoreDurability(element, new ItemStack(Items.CHAINMAIL_LEGGINGS))) {
                    temperature.fill(0.5f);
                } else if (player.ticksExisted % 450 == 0 && ItemStack.areItemsEqualIgnoreDurability(element, new ItemStack(Items.CHAINMAIL_CHESTPLATE))) {
                    temperature.fill(0.5f);
                } else if (player.ticksExisted % 450 == 0 && ItemStack.areItemsEqualIgnoreDurability(element, new ItemStack(Items.CHAINMAIL_HELMET))) {
                    temperature.fill(0.5f);
                }

                //철갑옷 세트
                if (player.ticksExisted % 450 == 0 && ItemStack.areItemsEqualIgnoreDurability(element, new ItemStack(Items.IRON_BOOTS))) {
                    temperature.fill(0.75f);
                } else if (player.ticksExisted % 450 == 0 && ItemStack.areItemsEqualIgnoreDurability(element, new ItemStack(Items.IRON_LEGGINGS))) {
                    temperature.fill(0.75f);
                } else if (player.ticksExisted % 450 == 0 && ItemStack.areItemsEqualIgnoreDurability(element, new ItemStack(Items.IRON_CHESTPLATE))) {
                    temperature.fill(0.75f);
                } else if (player.ticksExisted % 450 == 0 && ItemStack.areItemsEqualIgnoreDurability(element, new ItemStack(Items.IRON_HELMET))) {
                    temperature.fill(0.75f);
                }

                //다이아몬드 갑옷 세트
                if (player.ticksExisted % 450 == 0 && ItemStack.areItemsEqualIgnoreDurability(element, new ItemStack(Items.DIAMOND_BOOTS))) {
                    temperature.fill(1.25f);
                } else if (player.ticksExisted % 450 == 0 && ItemStack.areItemsEqualIgnoreDurability(element, new ItemStack(Items.DIAMOND_LEGGINGS))) {
                    temperature.fill(1.25f);
                } else if (player.ticksExisted % 450 == 0 && ItemStack.areItemsEqualIgnoreDurability(element, new ItemStack(Items.DIAMOND_CHESTPLATE))) {
                    temperature.fill(1.25f);
                } else if (player.ticksExisted % 450 == 0 && ItemStack.areItemsEqualIgnoreDurability(element, new ItemStack(Items.DIAMOND_HELMET))) {
                    temperature.fill(1.25f);
                }
            }

            // ================================
            //            수분
            // ================================
            //수분이 100이랑 같거나 이상일 경우
            if (thirst.getThirst() >= thirst.getMaxThirst()) {
                thirst.set(thirst.getThirst());
            }

            //5초마다 수분갑소
            if (player.ticksExisted % 100 == 0 && player.world.isRemote) {
                thirst.consume(1.0f);
            }
        }
    }
}
