package kr.guebeul.hertz.util.handler;

import kr.guebeul.hertz.packet.OpenInvPacket;
import kr.guebeul.hertz.packet.OpenRadioPacket;
import kr.guebeul.hertz.packet.SyncInventoryPacket;
import kr.guebeul.hertz.packet.SyncPlayerDataPacket;
import kr.guebeul.hertz.packet.fatigue.FatiguePacket;
import kr.guebeul.hertz.packet.food.FoodPacket;
import kr.guebeul.hertz.packet.temp.Temp20Packet;
import kr.guebeul.hertz.packet.temp.Temp50Packet;
import kr.guebeul.hertz.packet.temp.Temp70Packet;
import kr.guebeul.hertz.packet.thirst.ThirstPacket;
import kr.guebeul.hertz.util.Reference;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class NetworkHandler {

    private static SimpleNetworkWrapper network;

    public static void init() {
        network = NetworkRegistry.INSTANCE.newSimpleChannel(Reference.MOD_ID);
        int packetID = 0;

        //GUI 관련
        network.registerMessage(OpenInvPacket.class, OpenInvPacket.class, packetID++, Side.SERVER);
        network.registerMessage(OpenRadioPacket.class, OpenRadioPacket.class, packetID++, Side.SERVER);
        network.registerMessage(SyncPlayerDataPacket.class, SyncPlayerDataPacket.class, SyncPlayerDataPacket.ID, Side.CLIENT);
        network.registerMessage(SyncInventoryPacket.class, SyncInventoryPacket.class, packetID++, Side.CLIENT);

        //요소 관련
        network.registerMessage(Temp70Packet.class, Temp70Packet.class, packetID++, Side.SERVER);
        network.registerMessage(Temp70Packet.Temp70PacketExit.class, Temp70Packet.Temp70PacketExit.class, packetID++, Side.SERVER);
        network.registerMessage(Temp50Packet.class, Temp50Packet.class, packetID++, Side.SERVER);
        network.registerMessage(Temp20Packet.class, Temp20Packet.class, packetID++, Side.SERVER);
        network.registerMessage(ThirstPacket.class, ThirstPacket.class, packetID++, Side.SERVER);
        network.registerMessage(ThirstPacket.ThirstPacketExit.class, ThirstPacket.ThirstPacketExit.class, packetID++, Side.SERVER);
        network.registerMessage(FoodPacket.class, FoodPacket.class, packetID++, Side.SERVER);
        network.registerMessage(FoodPacket.FoodPacketExit.class, FoodPacket.FoodPacketExit.class, packetID++, Side.SERVER);
        network.registerMessage(FatiguePacket.class, FatiguePacket.class, packetID++, Side.SERVER);
        network.registerMessage(FatiguePacket.FatiguePacketExit.class, FatiguePacket.FatiguePacketExit.class, packetID++, Side.SERVER);
    }

    public static void sendToServer(IMessage message) {
        network.sendToServer(message);
    }
}
