package kr.guebeul.hertz.util.handler;

import kr.guebeul.hertz.Hertz;
import kr.guebeul.hertz.util.Reference;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class SoundsHandler {

    public static SoundEvent NUMBER_00317;
    public static SoundEvent NUMBER_10109;
    public static SoundEvent NUMBER_12221;
    public static SoundEvent NUMBER_18905;
    public static SoundEvent NUMBER_21442;
    public static SoundEvent NUMBER_24245;
    public static SoundEvent NUMBER_33112;
    public static SoundEvent NUMBER_34221;
    public static SoundEvent NUMBER_44551;
    public static SoundEvent NUMBER_54311;
    public static SoundEvent NUMBER_55553;
    public static SoundEvent NUMBER_69830;
    public static SoundEvent NUMBER_88818;
    public static SoundEvent NUMBER_99645;

    public static void init() {
        NUMBER_00317 = registerSound("voice.radio.00317");
        NUMBER_10109 = registerSound("voice.radio.10109");
        NUMBER_12221 = registerSound("voice.radio.12221");
        NUMBER_18905 = registerSound("voice.radio.18905");
        NUMBER_21442 = registerSound("voice.radio.21442");
        NUMBER_24245 = registerSound("voice.radio.24245");
        NUMBER_33112 = registerSound("voice.radio.33112");
        NUMBER_34221 = registerSound("voice.radio.34221");
        NUMBER_44551 = registerSound("voice.radio.44551");
        NUMBER_54311 = registerSound("voice.radio.54311");
        NUMBER_55553 = registerSound("voice.radio.55553");
        NUMBER_69830 = registerSound("voice.radio.69830");
        NUMBER_88818 = registerSound("voice.radio.88818");
        NUMBER_99645 = registerSound("voice.radio.99645");
    }

    private static SoundEvent registerSound(String name) {
        ResourceLocation location = new ResourceLocation(Reference.MOD_ID, name);
        SoundEvent event = new SoundEvent(location);
        event.setRegistryName(name);
        ForgeRegistries.SOUND_EVENTS.register(event);
        return event;
    }

}
