package kr.guebeul.hertz.util.timer;

public class HertzTimer extends Thread {

    private long time;
    private HertzTimerEvent e;

    public HertzTimer(long time, HertzTimerEvent e) {
        this.time = time;
        this.e = e;
        this.start();
        this.run();
    }

    @Override
    public void run() {
        try {
            Thread.sleep(time);
            e.run();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
