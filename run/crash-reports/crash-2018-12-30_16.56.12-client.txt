---- Minecraft Crash Report ----
// You're mean.

Time: 12/30/18 4:56 PM
Description: Rendering entity in world

java.lang.NullPointerException: Rendering entity in world
	at net.minecraft.client.renderer.entity.RenderLivingBase.canRenderName(RenderLivingBase.java:536)
	at net.minecraft.client.renderer.entity.RenderLiving.canRenderName(RenderLiving.java:26)
	at net.minecraft.client.renderer.entity.RenderLiving.canRenderName(RenderLiving.java:16)
	at net.minecraft.client.renderer.entity.RenderLivingBase.renderName(RenderLivingBase.java:491)
	at net.minecraft.client.renderer.entity.RenderLivingBase.renderName(RenderLivingBase.java:25)
	at net.minecraft.client.renderer.entity.Render.doRender(Render.java:67)
	at net.minecraft.client.renderer.entity.RenderLivingBase.doRender(RenderLivingBase.java:210)
	at net.minecraft.client.renderer.entity.RenderLiving.doRender(RenderLiving.java:51)
	at net.minecraft.client.renderer.entity.RenderLiving.doRender(RenderLiving.java:16)
	at net.minecraft.client.renderer.entity.RenderManager.renderEntity(RenderManager.java:390)
	at net.minecraft.client.renderer.entity.RenderManager.renderEntityStatic(RenderManager.java:374)
	at net.minecraft.client.renderer.RenderGlobal.renderEntities(RenderGlobal.java:655)
	at net.minecraft.client.renderer.EntityRenderer.renderWorldPass(EntityRenderer.java:1400)
	at net.minecraft.client.renderer.EntityRenderer.renderWorld(EntityRenderer.java:1312)
	at net.minecraft.client.renderer.EntityRenderer.updateCameraAndRender(EntityRenderer.java:1115)
	at net.minecraft.client.Minecraft.runGameLoop(Minecraft.java:1208)
	at net.minecraft.client.Minecraft.run(Minecraft.java:441)
	at net.minecraft.client.main.Main.main(Main.java:118)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraftforge.gradle.GradleStartCommon.launch(GradleStartCommon.java:97)
	at GradleStart.main(GradleStart.java:25)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Client thread
Stacktrace:
	at net.minecraft.client.renderer.entity.RenderLivingBase.canRenderName(RenderLivingBase.java:536)
	at net.minecraft.client.renderer.entity.RenderLiving.canRenderName(RenderLiving.java:26)
	at net.minecraft.client.renderer.entity.RenderLiving.canRenderName(RenderLiving.java:16)
	at net.minecraft.client.renderer.entity.RenderLivingBase.renderName(RenderLivingBase.java:491)
	at net.minecraft.client.renderer.entity.RenderLivingBase.renderName(RenderLivingBase.java:25)
	at net.minecraft.client.renderer.entity.Render.doRender(Render.java:67)
	at net.minecraft.client.renderer.entity.RenderLivingBase.doRender(RenderLivingBase.java:210)
	at net.minecraft.client.renderer.entity.RenderLiving.doRender(RenderLiving.java:51)
	at net.minecraft.client.renderer.entity.RenderLiving.doRender(RenderLiving.java:16)

-- Entity being rendered --
Details:
	Entity Type: hertz:bear (kr.guebeul.hertz.entity.bear.EntityBear)
	Entity ID: 61
	Entity Name: entity.bear.name
	Entity's Exact location: -462.93, 4.00, 1560.39
	Entity's Block location: World: (-463,4,1560), Chunk: (at 1,0,8 in -29,97; contains blocks -464,0,1552 to -449,255,1567), Region: (-1,3; contains chunks -32,96 to -1,127, blocks -512,0,1536 to -1,255,2047)
	Entity's Momentum: 0.05, -0.07, 0.02
	Entity's Passengers: []
	Entity's Vehicle: ~~ERROR~~ NullPointerException: null

-- Renderer details --
Details:
	Assigned renderer: kr.guebeul.hertz.entity.bear.RenderBear@830b8a5
	Location: 7.16,-7.00,-0.20 - World: (7,-7,-1), Chunk: (at 7,-1,15 in 0,-1; contains blocks 0,0,-16 to 15,255,-1), Region: (0,-1; contains chunks 0,-32 to 31,-1, blocks 0,0,-512 to 511,255,-1)
	Rotation: -71.71875
	Delta: 0.8599987
Stacktrace:
	at net.minecraft.client.renderer.entity.RenderManager.renderEntity(RenderManager.java:390)
	at net.minecraft.client.renderer.entity.RenderManager.renderEntityStatic(RenderManager.java:374)
	at net.minecraft.client.renderer.RenderGlobal.renderEntities(RenderGlobal.java:655)
	at net.minecraft.client.renderer.EntityRenderer.renderWorldPass(EntityRenderer.java:1400)
	at net.minecraft.client.renderer.EntityRenderer.renderWorld(EntityRenderer.java:1312)

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [EntityPlayerSP['Player956'/42, l='MpServer', x=-470.08, y=11.00, z=1560.59]]
	Chunk stats: MultiplayerChunkCache: 595, 595
	Level seed: 0
	Level generator: ID 01 - flat, ver 0. Features enabled: false
	Level generator options: 
	Level spawn location: World: (-698,4,1696), Chunk: (at 6,0,0 in -44,106; contains blocks -704,0,1696 to -689,255,1711), Region: (-2,3; contains chunks -64,96 to -33,127, blocks -1024,0,1536 to -513,255,2047)
	Level time: 125848 game time, 5826 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: false
	Forced entities: 38 total; [EntityVillager['Villager'/64, l='MpServer', x=-472.93, y=5.00, z=1536.91], EntityVillager['Villager'/65, l='MpServer', x=-476.80, y=4.94, z=1538.68], EntityVillager['Villager'/66, l='MpServer', x=-477.17, y=4.94, z=1540.47], EntityVillager['Villager'/67, l='MpServer', x=-478.43, y=5.00, z=1538.49], EntityVillager['Villager'/68, l='MpServer', x=-470.50, y=4.00, z=1540.09], EntityVillager['Villager'/69, l='MpServer', x=-468.70, y=7.00, z=1523.70], EntityVillager['Villager'/70, l='MpServer', x=-471.32, y=3.94, z=1528.56], EntityVillager['Villager'/71, l='MpServer', x=-472.71, y=4.64, z=1533.41], EntityVillager['Villager'/72, l='MpServer', x=-479.14, y=5.00, z=1538.17], EntitySlime['Slime'/78, l='MpServer', x=-446.02, y=5.25, z=1596.68], EntityVillager['Villager'/80, l='MpServer', x=-441.75, y=3.94, z=1555.40], EntityVillager['Villager'/81, l='MpServer', x=-434.35, y=5.00, z=1557.50], EntityVillager['Villager'/82, l='MpServer', x=-429.51, y=6.00, z=1561.69], EntityItem['item.item.wheat'/2720, l='MpServer', x=-476.62, y=5.30, z=1541.09], EntityItem['item.item.seeds'/2721, l='MpServer', x=-478.10, y=5.06, z=1539.79], EntityItem['item.item.seeds'/2722, l='MpServer', x=-478.32, y=5.02, z=1539.73], EntitySlime['Slime'/39, l='MpServer', x=-538.29, y=5.25, z=1510.29], EntitySlime['Slime'/43, l='MpServer', x=-479.32, y=5.18, z=1502.53], EntitySlime['Slime'/45, l='MpServer', x=-508.85, y=4.00, z=1498.56], EntitySlime['Slime'/46, l='MpServer', x=-541.66, y=4.00, z=1502.93], EntityItem['item.item.seeds'/47, l='MpServer', x=-469.09, y=5.00, z=1587.09], EntityItem['item.item.seeds'/48, l='MpServer', x=-465.48, y=4.00, z=1584.25], EntityVillager['Villager'/49, l='MpServer', x=-468.73, y=5.00, z=1589.03], EntityVillager['Villager'/50, l='MpServer', x=-453.65, y=4.00, z=1541.50], EntityVillager['Villager'/51, l='MpServer', x=-455.48, y=4.00, z=1550.60], EntityVillager['Villager'/52, l='MpServer', x=-462.30, y=5.00, z=1523.70], EntityVillager['Villager'/53, l='MpServer', x=-456.70, y=5.00, z=1526.70], EntityVillager['Villager'/54, l='MpServer', x=-456.70, y=5.00, z=1524.30], EntityVillager['Villager'/55, l='MpServer', x=-463.20, y=5.00, z=1523.20], EntityVillager['Villager'/56, l='MpServer', x=-463.21, y=5.00, z=1521.93], EntityVillager['Villager'/57, l='MpServer', x=-451.99, y=5.00, z=1561.83], EntityVillager['Villager'/58, l='MpServer', x=-456.64, y=4.00, z=1553.26], EntityVillager['Villager'/59, l='MpServer', x=-451.07, y=5.00, z=1561.04], EntityVillager['Villager'/60, l='MpServer', x=-456.80, y=4.00, z=1566.51], EntityPlayerSP['Player956'/42, l='MpServer', x=-470.08, y=11.00, z=1560.59], EntityBear['entity.bear.name'/61, l='MpServer', x=-462.93, y=4.00, z=1560.39], EntityVillager['Villager'/62, l='MpServer', x=-469.50, y=4.00, z=1572.39], EntityVillager['Villager'/63, l='MpServer', x=-482.88, y=4.00, z=1548.22]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.multiplayer.WorldClient.addWorldInfoToCrashReport(WorldClient.java:461)
	at net.minecraft.client.Minecraft.addGraphicsAndWorldToCrashReport(Minecraft.java:2888)
	at net.minecraft.client.Minecraft.run(Minecraft.java:462)
	at net.minecraft.client.main.Main.main(Main.java:118)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraftforge.gradle.GradleStartCommon.launch(GradleStartCommon.java:97)
	at GradleStart.main(GradleStart.java:25)

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_192, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 291991784 bytes (278 MB) / 748683264 bytes (714 MB) up to 1900019712 bytes (1812 MB)
	JVM Flags: 0 total; 
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP 9.42 Powered by Forge 14.23.5.2768 6 mods loaded, 6 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	| State     | ID           | Version      | Source                              | Signature |
	|:--------- |:------------ |:------------ |:----------------------------------- |:--------- |
	| UCHIJAAAA | minecraft    | 1.12.2       | minecraft.jar                       | None      |
	| UCHIJAAAA | mcp          | 9.42         | minecraft.jar                       | None      |
	| UCHIJAAAA | FML          | 8.0.99.99    | forgeSrc-1.12.2-14.23.5.2768.jar    | None      |
	| UCHIJAAAA | forge        | 14.23.5.2768 | forgeSrc-1.12.2-14.23.5.2768.jar    | None      |
	| UCHIJAAAA | hertz        | ${version}   | kr.guebeul.hertz.hertz-project.main | None      |
	| UCHIJAAAA | obsidian_api | 1.0.0        | ObsidianAPI-1.0.0.jar               | None      |

	Loaded coremods (and transformers): 
	GL info: ' Vendor: 'NVIDIA Corporation' Version: '4.6.0 NVIDIA 417.35' Renderer: 'GeForce GTX 1050/PCIe/SSE2'
	Launched Version: 1.12.2
	LWJGL: 2.9.4
	OpenGL: GeForce GTX 1050/PCIe/SSE2 GL version 4.6.0 NVIDIA 417.35, NVIDIA Corporation
	GL Caps: Using GL 1.3 multitexturing.
Using GL 1.3 texture combiners.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Shaders are available because OpenGL 2.1 is supported.
VBOs are available because OpenGL 1.5 is supported.

	Using VBOs: Yes
	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: 
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	CPU: 8x AMD Ryzen 5 1500X Quad-Core Processor 