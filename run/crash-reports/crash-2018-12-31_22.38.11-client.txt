---- Minecraft Crash Report ----
// You're mean.

Time: 12/31/18 10:38 PM
Description: Unexpected error

java.lang.NullPointerException: Unexpected error
	at kr.guebeul.hertz.gui.HertzHUD.renderOverlay(HertzHUD.java:37)
	at net.minecraftforge.fml.common.eventhandler.ASMEventHandler_18_HertzHUD_renderOverlay_RenderGameOverlayEvent.invoke(.dynamic)
	at net.minecraftforge.fml.common.eventhandler.ASMEventHandler.invoke(ASMEventHandler.java:90)
	at net.minecraftforge.fml.common.eventhandler.EventBus.post(EventBus.java:182)
	at net.minecraftforge.client.GuiIngameForge.pre(GuiIngameForge.java:910)
	at net.minecraftforge.client.GuiIngameForge.renderGameOverlay(GuiIngameForge.java:119)
	at net.minecraft.client.renderer.EntityRenderer.updateCameraAndRender(EntityRenderer.java:1151)
	at net.minecraft.client.Minecraft.runGameLoop(Minecraft.java:1208)
	at net.minecraft.client.Minecraft.run(Minecraft.java:441)
	at net.minecraft.client.main.Main.main(Main.java:118)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraftforge.gradle.GradleStartCommon.launch(GradleStartCommon.java:97)
	at GradleStart.main(GradleStart.java:25)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Client thread
Stacktrace:
	at kr.guebeul.hertz.gui.HertzHUD.renderOverlay(HertzHUD.java:37)
	at net.minecraftforge.fml.common.eventhandler.ASMEventHandler_18_HertzHUD_renderOverlay_RenderGameOverlayEvent.invoke(.dynamic)
	at net.minecraftforge.fml.common.eventhandler.ASMEventHandler.invoke(ASMEventHandler.java:90)
	at net.minecraftforge.fml.common.eventhandler.EventBus.post(EventBus.java:182)
	at net.minecraftforge.client.GuiIngameForge.pre(GuiIngameForge.java:910)
	at net.minecraftforge.client.GuiIngameForge.renderGameOverlay(GuiIngameForge.java:119)

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [EntityPlayerSP['Player411'/222, l='MpServer', x=8.50, y=65.00, z=8.50]]
	Chunk stats: MultiplayerChunkCache: 0, 0
	Level seed: 0
	Level generator: ID 00 - default, ver 1. Features enabled: false
	Level generator options: 
	Level spawn location: World: (8,64,8), Chunk: (at 8,4,8 in 0,0; contains blocks 0,0,0 to 15,255,15), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 0 game time, 0 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: survival (ID 0). Hardcore: false. Cheats: false
	Forced entities: 1 total; [EntityPlayerSP['Player411'/222, l='MpServer', x=8.50, y=65.00, z=8.50]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.multiplayer.WorldClient.addWorldInfoToCrashReport(WorldClient.java:461)
	at net.minecraft.client.Minecraft.addGraphicsAndWorldToCrashReport(Minecraft.java:2888)
	at net.minecraft.client.Minecraft.run(Minecraft.java:470)
	at net.minecraft.client.main.Main.main(Main.java:118)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraftforge.gradle.GradleStartCommon.launch(GradleStartCommon.java:97)
	at GradleStart.main(GradleStart.java:25)

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_192, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 434394144 bytes (414 MB) / 730857472 bytes (697 MB) up to 1900019712 bytes (1812 MB)
	JVM Flags: 0 total; 
	IntCache: cache: 0, tcache: 0, allocated: 13, tallocated: 95
	FML: MCP 9.42 Powered by Forge 14.23.5.2768 6 mods loaded, 6 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	| State     | ID           | Version      | Source                              | Signature |
	|:--------- |:------------ |:------------ |:----------------------------------- |:--------- |
	| UCHIJAAAA | minecraft    | 1.12.2       | minecraft.jar                       | None      |
	| UCHIJAAAA | mcp          | 9.42         | minecraft.jar                       | None      |
	| UCHIJAAAA | FML          | 8.0.99.99    | forgeSrc-1.12.2-14.23.5.2768.jar    | None      |
	| UCHIJAAAA | forge        | 14.23.5.2768 | forgeSrc-1.12.2-14.23.5.2768.jar    | None      |
	| UCHIJAAAA | hertz        | ${version}   | kr.guebeul.hertz.hertz-project.main | None      |
	| UCHIJAAAA | obsidian_api | 1.0.0        | ObsidianAPI-1.0.0.jar               | None      |

	Loaded coremods (and transformers): 
	GL info: ' Vendor: 'NVIDIA Corporation' Version: '4.6.0 NVIDIA 417.35' Renderer: 'GeForce GTX 1050/PCIe/SSE2'
	Launched Version: 1.12.2
	LWJGL: 2.9.4
	OpenGL: GeForce GTX 1050/PCIe/SSE2 GL version 4.6.0 NVIDIA 417.35, NVIDIA Corporation
	GL Caps: Using GL 1.3 multitexturing.
Using GL 1.3 texture combiners.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Shaders are available because OpenGL 2.1 is supported.
VBOs are available because OpenGL 1.5 is supported.

	Using VBOs: Yes
	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: 
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	CPU: 8x AMD Ryzen 5 1500X Quad-Core Processor 