---- Minecraft Crash Report ----
// Don't be sad. I'll do better next time, I promise!

Time: 1/1/19 12:37 PM
Description: Ticking player

java.lang.NullPointerException: Ticking player
	at kr.guebeul.hertz.util.handler.EventHandler.onLivingUpdate(EventHandler.java:42)
	at net.minecraftforge.fml.common.eventhandler.ASMEventHandler_16_EventHandler_onLivingUpdate_PlayerTickEvent.invoke(.dynamic)
	at net.minecraftforge.fml.common.eventhandler.ASMEventHandler.invoke(ASMEventHandler.java:90)
	at net.minecraftforge.fml.common.eventhandler.EventBus.post(EventBus.java:182)
	at net.minecraftforge.fml.common.FMLCommonHandler.onPlayerPreTick(FMLCommonHandler.java:365)
	at net.minecraft.entity.player.EntityPlayer.onUpdate(EntityPlayer.java:228)
	at net.minecraft.entity.player.EntityPlayerMP.onUpdateEntity(EntityPlayerMP.java:423)
	at net.minecraft.network.NetHandlerPlayServer.update(NetHandlerPlayServer.java:185)
	at net.minecraftforge.fml.common.network.handshake.NetworkDispatcher$1.update(NetworkDispatcher.java:212)
	at net.minecraft.network.NetworkManager.processReceivedPackets(NetworkManager.java:307)
	at net.minecraft.network.NetworkSystem.networkTick(NetworkSystem.java:197)
	at net.minecraft.server.MinecraftServer.updateTimeLightAndEntities(MinecraftServer.java:865)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:743)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:192)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:592)
	at java.lang.Thread.run(Thread.java:748)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Server thread
Stacktrace:
	at kr.guebeul.hertz.util.handler.EventHandler.onLivingUpdate(EventHandler.java:42)
	at net.minecraftforge.fml.common.eventhandler.ASMEventHandler_16_EventHandler_onLivingUpdate_PlayerTickEvent.invoke(.dynamic)
	at net.minecraftforge.fml.common.eventhandler.ASMEventHandler.invoke(ASMEventHandler.java:90)
	at net.minecraftforge.fml.common.eventhandler.EventBus.post(EventBus.java:182)
	at net.minecraftforge.fml.common.FMLCommonHandler.onPlayerPreTick(FMLCommonHandler.java:365)
	at net.minecraft.entity.player.EntityPlayer.onUpdate(EntityPlayer.java:228)

-- Player being ticked --
Details:
	Entity Type: null (net.minecraft.entity.player.EntityPlayerMP)
	Entity ID: 276
	Entity Name: Player898
	Entity's Exact location: 46.64, 63.00, -57.66
	Entity's Block location: World: (46,63,-58), Chunk: (at 14,3,6 in 2,-4; contains blocks 32,0,-64 to 47,255,-49), Region: (0,-1; contains chunks 0,-32 to 31,-1, blocks 0,0,-512 to 511,255,-1)
	Entity's Momentum: 0.00, -0.08, 0.00
	Entity's Passengers: []
	Entity's Vehicle: ~~ERROR~~ NullPointerException: null
Stacktrace:
	at net.minecraft.entity.player.EntityPlayerMP.onUpdateEntity(EntityPlayerMP.java:423)
	at net.minecraft.network.NetHandlerPlayServer.update(NetHandlerPlayServer.java:185)
	at net.minecraftforge.fml.common.network.handshake.NetworkDispatcher$1.update(NetworkDispatcher.java:212)
	at net.minecraft.network.NetworkManager.processReceivedPackets(NetworkManager.java:307)

-- Ticking connection --
Details:
	Connection: net.minecraft.network.NetworkManager@5eb0ed78
Stacktrace:
	at net.minecraft.network.NetworkSystem.networkTick(NetworkSystem.java:197)
	at net.minecraft.server.MinecraftServer.updateTimeLightAndEntities(MinecraftServer.java:865)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:743)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:192)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:592)
	at java.lang.Thread.run(Thread.java:748)

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_192, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 300093320 bytes (286 MB) / 696778752 bytes (664 MB) up to 1900019712 bytes (1812 MB)
	JVM Flags: 0 total; 
	IntCache: cache: 0, tcache: 0, allocated: 12, tallocated: 94
	FML: MCP 9.42 Powered by Forge 14.23.5.2768 6 mods loaded, 6 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	| State     | ID           | Version      | Source                              | Signature |
	|:--------- |:------------ |:------------ |:----------------------------------- |:--------- |
	| UCHIJAAAA | minecraft    | 1.12.2       | minecraft.jar                       | None      |
	| UCHIJAAAA | mcp          | 9.42         | minecraft.jar                       | None      |
	| UCHIJAAAA | FML          | 8.0.99.99    | forgeSrc-1.12.2-14.23.5.2768.jar    | None      |
	| UCHIJAAAA | forge        | 14.23.5.2768 | forgeSrc-1.12.2-14.23.5.2768.jar    | None      |
	| UCHIJAAAA | hertz        | ${version}   | kr.guebeul.hertz.hertz-project.main | None      |
	| UCHIJAAAA | obsidian_api | 1.0.0        | ObsidianAPI-1.0.0.jar               | None      |

	Loaded coremods (and transformers): 
	GL info: ~~ERROR~~ RuntimeException: No OpenGL context found in the current thread.
	Profiler Position: N/A (disabled)
	Player Count: 1 / 8; [EntityPlayerMP['Player898'/276, l='New World', x=46.64, y=63.00, z=-57.66]]
	Type: Integrated Server (map_client.txt)
	Is Modded: Definitely; Client brand changed to 'fml,forge'