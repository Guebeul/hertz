---- Minecraft Crash Report ----
// You're mean.

Time: 1/2/19 3:10 PM
Description: Rendering entity in world

java.lang.NullPointerException: Rendering entity in world
	at net.minecraft.client.renderer.entity.RenderLivingBase.canRenderName(RenderLivingBase.java:536)
	at net.minecraft.client.renderer.entity.RenderLiving.canRenderName(RenderLiving.java:26)
	at net.minecraft.client.renderer.entity.RenderLiving.canRenderName(RenderLiving.java:16)
	at net.minecraft.client.renderer.entity.RenderLivingBase.renderName(RenderLivingBase.java:491)
	at net.minecraft.client.renderer.entity.RenderLivingBase.renderName(RenderLivingBase.java:25)
	at net.minecraft.client.renderer.entity.Render.doRender(Render.java:67)
	at net.minecraft.client.renderer.entity.RenderLivingBase.doRender(RenderLivingBase.java:210)
	at net.minecraft.client.renderer.entity.RenderLiving.doRender(RenderLiving.java:51)
	at net.minecraft.client.renderer.entity.RenderLiving.doRender(RenderLiving.java:16)
	at net.minecraft.client.renderer.entity.RenderManager.renderEntity(RenderManager.java:390)
	at net.minecraft.client.renderer.entity.RenderManager.renderEntityStatic(RenderManager.java:374)
	at net.minecraft.client.renderer.RenderGlobal.renderEntities(RenderGlobal.java:655)
	at net.minecraft.client.renderer.EntityRenderer.renderWorldPass(EntityRenderer.java:1400)
	at net.minecraft.client.renderer.EntityRenderer.renderWorld(EntityRenderer.java:1312)
	at net.minecraft.client.renderer.EntityRenderer.updateCameraAndRender(EntityRenderer.java:1115)
	at net.minecraft.client.Minecraft.runGameLoop(Minecraft.java:1208)
	at net.minecraft.client.Minecraft.run(Minecraft.java:441)
	at net.minecraft.client.main.Main.main(Main.java:118)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraftforge.gradle.GradleStartCommon.launch(GradleStartCommon.java:97)
	at GradleStart.main(GradleStart.java:25)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Client thread
Stacktrace:
	at net.minecraft.client.renderer.entity.RenderLivingBase.canRenderName(RenderLivingBase.java:536)
	at net.minecraft.client.renderer.entity.RenderLiving.canRenderName(RenderLiving.java:26)
	at net.minecraft.client.renderer.entity.RenderLiving.canRenderName(RenderLiving.java:16)
	at net.minecraft.client.renderer.entity.RenderLivingBase.renderName(RenderLivingBase.java:491)
	at net.minecraft.client.renderer.entity.RenderLivingBase.renderName(RenderLivingBase.java:25)
	at net.minecraft.client.renderer.entity.Render.doRender(Render.java:67)
	at net.minecraft.client.renderer.entity.RenderLivingBase.doRender(RenderLivingBase.java:210)
	at net.minecraft.client.renderer.entity.RenderLiving.doRender(RenderLiving.java:51)
	at net.minecraft.client.renderer.entity.RenderLiving.doRender(RenderLiving.java:16)

-- Entity being rendered --
Details:
	Entity Type: hertz:bear (kr.guebeul.hertz.entity.EntityBear)
	Entity ID: 153
	Entity Name: §l
	Entity's Exact location: 57.48, 66.00, -91.17
	Entity's Block location: World: (57,66,-92), Chunk: (at 9,4,4 in 3,-6; contains blocks 48,0,-96 to 63,255,-81), Region: (0,-1; contains chunks 0,-32 to 31,-1, blocks 0,0,-512 to 511,255,-1)
	Entity's Momentum: 0.00, -0.06, 0.00
	Entity's Passengers: []
	Entity's Vehicle: ~~ERROR~~ NullPointerException: null

-- Renderer details --
Details:
	Assigned renderer: kr.guebeul.hertz.entity.RenderBear@1f97ea36
	Location: -6.59,-2.00,1.80 - World: (-7,-2,1), Chunk: (at 9,-1,1 in -1,0; contains blocks -16,0,0 to -1,255,15), Region: (-1,0; contains chunks -32,0 to -1,31, blocks -512,0,0 to -1,255,511)
	Rotation: 113.90625
	Delta: 0.13999939
Stacktrace:
	at net.minecraft.client.renderer.entity.RenderManager.renderEntity(RenderManager.java:390)
	at net.minecraft.client.renderer.entity.RenderManager.renderEntityStatic(RenderManager.java:374)
	at net.minecraft.client.renderer.RenderGlobal.renderEntities(RenderGlobal.java:655)
	at net.minecraft.client.renderer.EntityRenderer.renderWorldPass(EntityRenderer.java:1400)
	at net.minecraft.client.renderer.EntityRenderer.renderWorld(EntityRenderer.java:1312)

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [EntityPlayerSP['Player461'/273, l='MpServer', x=64.07, y=68.00, z=-92.97]]
	Chunk stats: MultiplayerChunkCache: 625, 625
	Level seed: 0
	Level generator: ID 00 - default, ver 1. Features enabled: false
	Level generator options: 
	Level spawn location: World: (0,0,0), Chunk: (at 0,0,0 in 0,0; contains blocks 0,0,0 to 15,255,15), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 305178 game time, 1000 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: false
	Forced entities: 103 total; [EntityChicken['Chicken'/65, l='MpServer', x=-10.39, y=63.00, z=-166.14], EntityZombie['Zombie'/66, l='MpServer', x=-4.43, y=53.00, z=-146.23], EntityCreeper['Creeper'/67, l='MpServer', x=-10.50, y=21.00, z=-135.50], EntitySkeleton['Skeleton'/68, l='MpServer', x=-2.50, y=22.00, z=-116.50], EntityCreeper['Creeper'/69, l='MpServer', x=-1.82, y=37.00, z=-51.52], EntityCreeper['Creeper'/70, l='MpServer', x=-6.50, y=41.00, z=-54.50], EntityCreeper['Creeper'/71, l='MpServer', x=-11.50, y=23.00, z=-36.50], EntityCreeper['Creeper'/72, l='MpServer', x=-14.50, y=23.00, z=-39.50], EntitySkeleton['Skeleton'/73, l='MpServer', x=-7.50, y=28.00, z=-32.50], EntityZombie['Zombie'/74, l='MpServer', x=0.56, y=20.00, z=-31.75], EntityCreeper['Creeper'/75, l='MpServer', x=-0.50, y=20.00, z=-39.50], EntitySkeleton['Skeleton'/76, l='MpServer', x=-9.50, y=25.00, z=-36.50], EntitySkeleton['Skeleton'/77, l='MpServer', x=-0.50, y=16.00, z=-29.50], EntityCreeper['Creeper'/78, l='MpServer', x=-3.50, y=16.00, z=-30.50], EntitySkeleton['Skeleton'/79, l='MpServer', x=-3.50, y=28.00, z=-20.50], EntityZombie['Zombie'/80, l='MpServer', x=-5.50, y=28.00, z=-26.50], EntityCreeper['Creeper'/84, l='MpServer', x=4.18, y=52.00, z=-126.43], EntitySkeleton['Skeleton'/85, l='MpServer', x=7.50, y=23.00, z=-125.50], EntityBat['Bat'/86, l='MpServer', x=10.46, y=51.04, z=-119.30], EntitySkeleton['Skeleton'/87, l='MpServer', x=1.50, y=29.00, z=-100.50], EntityBat['Bat'/88, l='MpServer', x=15.87, y=26.10, z=-114.03], EntityCreeper['Creeper'/89, l='MpServer', x=2.50, y=21.00, z=-108.50], EntityItem['item.item.egg'/90, l='MpServer', x=15.99, y=61.00, z=-110.45], EntitySquid['Squid'/91, l='MpServer', x=8.18, y=59.83, z=-92.29], EntitySquid['Squid'/92, l='MpServer', x=8.15, y=58.64, z=-92.02], EntityBat['Bat'/93, l='MpServer', x=4.83, y=12.10, z=-78.07], EntityBat['Bat'/94, l='MpServer', x=2.50, y=13.45, z=-72.35], EntityBat['Bat'/95, l='MpServer', x=14.58, y=14.40, z=-47.48], EntityCreeper['Creeper'/96, l='MpServer', x=0.54, y=28.00, z=-33.45], EntityBat['Bat'/97, l='MpServer', x=9.43, y=13.29, z=-43.32], EntityZombieVillager['Zombie Villager'/98, l='MpServer', x=0.50, y=28.00, z=-16.50], EntitySkeleton['Skeleton'/102, l='MpServer', x=31.87, y=13.00, z=-146.90], EntitySkeleton['Skeleton'/103, l='MpServer', x=24.50, y=34.00, z=-155.50], EntityZombie['Zombie'/104, l='MpServer', x=25.50, y=34.00, z=-153.50], EntityBat['Bat'/105, l='MpServer', x=24.34, y=35.16, z=-130.75], EntityCreeper['Creeper'/106, l='MpServer', x=19.50, y=18.00, z=-115.50], EntitySpider['Spider'/107, l='MpServer', x=22.70, y=40.00, z=-124.00], EntityBat['Bat'/108, l='MpServer', x=19.93, y=18.92, z=-98.10], EntityChicken['Chicken'/109, l='MpServer', x=16.52, y=61.00, z=-111.22], EntityCreeper['Creeper'/110, l='MpServer', x=28.50, y=45.00, z=-82.79], EntityCreeper['Creeper'/111, l='MpServer', x=20.50, y=15.00, z=-59.50], EntityZombie['Zombie'/112, l='MpServer', x=19.50, y=15.00, z=-58.50], EntitySkeleton['Skeleton'/113, l='MpServer', x=28.78, y=24.00, z=-49.48], EntityZombie['Zombie'/114, l='MpServer', x=17.74, y=16.00, z=-59.50], EntityBat['Bat'/115, l='MpServer', x=14.35, y=15.09, z=-51.65], EntityPlayerSP['Player461'/273, l='MpServer', x=64.07, y=68.00, z=-92.97], EntityItem['item.tile.dirt.default'/123, l='MpServer', x=19.79, y=63.00, z=-29.02], EntityItem['item.item.seeds'/125, l='MpServer', x=19.05, y=64.00, z=-29.59], EntityCreeper['Creeper'/132, l='MpServer', x=32.50, y=13.00, z=-146.50], EntityZombie['Zombie'/133, l='MpServer', x=42.87, y=13.00, z=-156.50], EntitySkeleton['Skeleton'/134, l='MpServer', x=43.50, y=13.00, z=-156.50], EntityBat['Bat'/135, l='MpServer', x=35.74, y=35.96, z=-138.19], EntityCreeper['Creeper'/136, l='MpServer', x=33.58, y=42.00, z=-78.19], EntityPig['Pig'/137, l='MpServer', x=46.74, y=64.00, z=-65.67], EntityBat['Bat'/138, l='MpServer', x=44.25, y=38.58, z=-58.75], EntityBat['Bat'/139, l='MpServer', x=46.33, y=43.10, z=-33.27], EntityZombie['Zombie'/140, l='MpServer', x=46.51, y=40.00, z=-37.80], EntityZombie['Zombie'/141, l='MpServer', x=44.79, y=43.00, z=-32.54], EntityPig['Pig'/142, l='MpServer', x=44.49, y=59.00, z=-42.72], EntityPig['Pig'/146, l='MpServer', x=61.73, y=67.00, z=-143.43], EntitySpider['Spider'/147, l='MpServer', x=49.50, y=37.00, z=-114.50], EntitySkeleton['Skeleton'/148, l='MpServer', x=62.50, y=38.00, z=-109.50], EntitySkeleton['Skeleton'/149, l='MpServer', x=56.72, y=38.00, z=-107.50], EntityZombie['Zombie'/150, l='MpServer', x=50.50, y=37.00, z=-107.50], EntityChicken['Chicken'/151, l='MpServer', x=56.50, y=70.00, z=-103.87], EntityItem['item.item.egg'/152, l='MpServer', x=49.93, y=67.00, z=-109.63], EntityBear['§l'/153, l='MpServer', x=57.48, y=66.00, z=-91.17], EntityCreeper['Creeper'/154, l='MpServer', x=56.50, y=33.00, z=-64.50], EntitySkeleton['Skeleton'/155, l='MpServer', x=53.30, y=34.06, z=-56.70], EntitySkeleton['Skeleton'/156, l='MpServer', x=55.27, y=33.00, z=-63.51], EntityBat['Bat'/157, l='MpServer', x=59.44, y=34.91, z=-55.70], EntityCreeper['Creeper'/158, l='MpServer', x=48.84, y=49.00, z=-62.48], EntityZombie['Zombie'/169, l='MpServer', x=75.19, y=37.00, z=-123.51], EntitySpider['Spider'/170, l='MpServer', x=69.95, y=37.00, z=-110.09], EntityItem['item.item.rottenFlesh'/171, l='MpServer', x=77.37, y=63.00, z=-42.85], EntityBat['Bat'/172, l='MpServer', x=67.26, y=49.10, z=-47.58], EntityChicken['Chicken'/179, l='MpServer', x=84.13, y=67.00, z=-143.48], EntityChicken['Chicken'/180, l='MpServer', x=92.50, y=61.00, z=-116.53], EntityItem['item.item.egg'/181, l='MpServer', x=92.13, y=61.00, z=-116.13], EntityBat['Bat'/182, l='MpServer', x=78.20, y=30.45, z=-113.51], EntityPig['Pig'/183, l='MpServer', x=82.29, y=65.00, z=-106.51], EntityPig['Pig'/184, l='MpServer', x=86.56, y=69.17, z=-75.49], EntitySkeleton['Skeleton'/185, l='MpServer', x=94.69, y=48.00, z=-36.50], EntityChicken['Chicken'/193, l='MpServer', x=96.59, y=66.00, z=-147.72], EntityPig['Pig'/194, l='MpServer', x=107.39, y=65.18, z=-72.46], EntityCreeper['Creeper'/195, l='MpServer', x=107.50, y=50.00, z=-36.50], EntitySkeleton['Skeleton'/196, l='MpServer', x=106.50, y=49.00, z=-36.50], EntityPig['Pig'/197, l='MpServer', x=97.77, y=63.00, z=-16.50], EntityChicken['Chicken'/204, l='MpServer', x=112.11, y=71.00, z=-167.57], EntityPig['Pig'/205, l='MpServer', x=116.27, y=68.00, z=-163.47], EntityCreeper['Creeper'/206, l='MpServer', x=117.50, y=39.00, z=-148.50], EntityChicken['Chicken'/207, l='MpServer', x=117.43, y=69.00, z=-159.01], EntityChicken['Chicken'/208, l='MpServer', x=115.17, y=70.00, z=-150.42], EntityPig['Pig'/209, l='MpServer', x=121.54, y=72.00, z=-140.26], EntityPig['Pig'/210, l='MpServer', x=126.48, y=67.00, z=-118.31], EntityPig['Pig'/214, l='MpServer', x=133.75, y=76.00, z=-149.49], EntityPig['Pig'/215, l='MpServer', x=136.54, y=76.00, z=-143.73], EntityBat['Bat'/216, l='MpServer', x=135.39, y=55.15, z=-112.89], EntityZombieVillager['Zombie Villager'/217, l='MpServer', x=130.50, y=46.00, z=-88.50], EntityZombieVillager['Zombie Villager'/218, l='MpServer', x=129.50, y=46.00, z=-89.50], EntityWitch['Witch'/219, l='MpServer', x=131.50, y=46.00, z=-89.50], EntitySkeleton['Skeleton'/220, l='MpServer', x=141.50, y=53.00, z=-37.50], EntityPig['Pig'/221, l='MpServer', x=141.25, y=68.00, z=-47.49]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.multiplayer.WorldClient.addWorldInfoToCrashReport(WorldClient.java:461)
	at net.minecraft.client.Minecraft.addGraphicsAndWorldToCrashReport(Minecraft.java:2888)
	at net.minecraft.client.Minecraft.run(Minecraft.java:462)
	at net.minecraft.client.main.Main.main(Main.java:118)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at net.minecraftforge.gradle.GradleStartCommon.launch(GradleStartCommon.java:97)
	at GradleStart.main(GradleStart.java:25)

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_192, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 216560304 bytes (206 MB) / 706215936 bytes (673 MB) up to 1900019712 bytes (1812 MB)
	JVM Flags: 0 total; 
	IntCache: cache: 0, tcache: 0, allocated: 13, tallocated: 95
	FML: MCP 9.42 Powered by Forge 14.23.5.2768 6 mods loaded, 6 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	| State     | ID           | Version      | Source                              | Signature |
	|:--------- |:------------ |:------------ |:----------------------------------- |:--------- |
	| UCHIJAAAA | minecraft    | 1.12.2       | minecraft.jar                       | None      |
	| UCHIJAAAA | mcp          | 9.42         | minecraft.jar                       | None      |
	| UCHIJAAAA | FML          | 8.0.99.99    | forgeSrc-1.12.2-14.23.5.2768.jar    | None      |
	| UCHIJAAAA | forge        | 14.23.5.2768 | forgeSrc-1.12.2-14.23.5.2768.jar    | None      |
	| UCHIJAAAA | hertz        | ${version}   | kr.guebeul.hertz.hertz-project.main | None      |
	| UCHIJAAAA | obsidian_api | 1.0.0        | ObsidianAPI-1.0.0.jar               | None      |

	Loaded coremods (and transformers): 
	GL info: ' Vendor: 'NVIDIA Corporation' Version: '4.6.0 NVIDIA 417.35' Renderer: 'GeForce GTX 1050/PCIe/SSE2'
	Launched Version: 1.12.2
	LWJGL: 2.9.4
	OpenGL: GeForce GTX 1050/PCIe/SSE2 GL version 4.6.0 NVIDIA 417.35, NVIDIA Corporation
	GL Caps: Using GL 1.3 multitexturing.
Using GL 1.3 texture combiners.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Shaders are available because OpenGL 2.1 is supported.
VBOs are available because OpenGL 1.5 is supported.

	Using VBOs: Yes
	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: 
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	CPU: 8x AMD Ryzen 5 1500X Quad-Core Processor 