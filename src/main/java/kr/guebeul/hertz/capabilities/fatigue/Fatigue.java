package kr.guebeul.hertz.capabilities.fatigue;

public class Fatigue implements IFatigue{

    private float fatigue = 0.0f;
    private float max_fatigue = 100.0f;
    private float min_fatigue = 0.0f;
    private float target_fatigue = 0.0f;

    @Override
    public void consume(float points) {
        this.fatigue -= points;

        if (fatigue > max_fatigue){
            fatigue = max_fatigue;
        }
        else if (fatigue < min_fatigue) {
            fatigue = min_fatigue;
        }
    }

    @Override
    public void fill(float points) {
        this.fatigue += points;

        if (fatigue > max_fatigue){
            fatigue = max_fatigue;
        }
        else if (fatigue < min_fatigue) {
            fatigue = min_fatigue;
        }
     }

    @Override
    public void set(float points) {
        this.fatigue = points;

        if (fatigue > max_fatigue){
            fatigue = max_fatigue;
        }
        else if (fatigue < min_fatigue) {
            fatigue = min_fatigue;
        }
    }

    @Override
    public void consumeTarget(float points) {
        this.fatigue -= points;
    }

    @Override
    public void fillTarget(float points) {
        this.fatigue += points;
    }

    @Override
    public void setTarget(float points) {
        this.fatigue = points;
    }

    @Override
    public float getFatigue() {
        return this.fatigue;
    }

    @Override
    public float getMaxFatigue() {
        return this.max_fatigue;
    }

    @Override
    public float getMinFatigue() {
        return this.min_fatigue;
    }

    @Override
    public float getTargetFatigue() {
        return this.target_fatigue;
    }
}
