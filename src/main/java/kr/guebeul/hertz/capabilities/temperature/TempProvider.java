package kr.guebeul.hertz.capabilities.temperature;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class TempProvider implements ICapabilitySerializable<NBTBase> {

    @CapabilityInject(ITemperature.class)
    public static final Capability<ITemperature> TEMP_CAP = null;

    private ITemperature instance = TEMP_CAP.getDefaultInstance();

    @Override
    public boolean hasCapability(@Nonnull Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == TEMP_CAP;
    }

    @Nullable
    @Override
    public <T> T getCapability(@Nonnull Capability<T> capability, @Nullable EnumFacing facing) {
        return capability == TEMP_CAP ? TEMP_CAP.<T> cast(this.instance) : null;
    }

    @Override
    public NBTBase serializeNBT() {
        return TEMP_CAP.getStorage().writeNBT(TEMP_CAP, this.instance, null);
    }

    @Override
    public void deserializeNBT(NBTBase nbt) {
        TEMP_CAP.getStorage().readNBT(TEMP_CAP, this.instance, null, nbt);
    }
}
