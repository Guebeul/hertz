package kr.guebeul.hertz.capabilities.temperature;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTPrimitive;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagFloat;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;

public class TempStorage implements Capability.IStorage<ITemperature> {

    private boolean temp = true;

    @Override
    public NBTBase writeNBT(Capability<ITemperature> capability, ITemperature instance, EnumFacing side) {
        if(temp = true)
            return new NBTTagFloat(instance.getTemp());
        else
            return new NBTTagFloat(instance.getMaxTemp());
    }

    @Override
    public void readNBT(Capability<ITemperature> capability, ITemperature instance, EnumFacing side, NBTBase nbt) {
        instance.set(((NBTPrimitive) nbt).getFloat());
        instance.setTarget(((NBTPrimitive) nbt).getFloat());
    }
}
