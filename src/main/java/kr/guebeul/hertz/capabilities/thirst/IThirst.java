package kr.guebeul.hertz.capabilities.thirst;

public interface IThirst {

    public void consume(float points);
    public void fill(float points);
    public void set(float points);

    public void consumeTarget(float points);
    public void fillTarget(float points);
    public void setTarget(float points);

    public float getThirst();
    public float getMaxThirst();
    public float getMinThirst();
    public float getTargetThirst();
}
