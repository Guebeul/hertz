package kr.guebeul.hertz.capabilities.thirst;

public class Thirst implements IThirst{

    private float Thirst = 100.0f;
    private float max_Thirst = 100.0f;
    private float min_Thirst = 0.0f;
    private float target_Thirst = 100.0f;

    @Override
    public void consume(float points) {
        this.Thirst -= points;

        if (Thirst > max_Thirst){
            Thirst = max_Thirst;
        }
        else if (Thirst < min_Thirst) {
            Thirst = min_Thirst;
        }
    }

    @Override
    public void fill(float points) {
        this.Thirst += points;

        if (Thirst > max_Thirst){
            Thirst = max_Thirst;
        }
        else if (Thirst < min_Thirst) {
            Thirst = min_Thirst;
        }
    }

    @Override
    public void set(float points) {
        this.Thirst = points;

        if (Thirst > max_Thirst){
            Thirst = max_Thirst;
        }
        else if (Thirst < min_Thirst) {
            Thirst = min_Thirst;
        }
    }

    @Override
    public void consumeTarget(float points) {
        this.Thirst -= points;
    }

    @Override
    public void fillTarget(float points) {
        this.Thirst += points;
    }

    @Override
    public void setTarget(float points) {
        this.Thirst = points;
    }

    @Override
    public float getThirst() {
        return this.Thirst;
    }

    @Override
    public float getMaxThirst() {
        return this.max_Thirst;
    }

    @Override
    public float getMinThirst() {
        return this.min_Thirst;
    }

    @Override
    public float getTargetThirst() {
        return this.target_Thirst;
    }
}
