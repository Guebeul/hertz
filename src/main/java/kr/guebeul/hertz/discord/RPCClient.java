package kr.guebeul.hertz.discord;

import club.minnced.discord.rpc.DiscordEventHandlers;
import club.minnced.discord.rpc.DiscordRPC;
import club.minnced.discord.rpc.DiscordRichPresence;
import net.minecraft.client.Minecraft;

import javax.annotation.Nullable;

public class RPCClient {

    public String CLIENT_ID;

    public RPCClient(String clientId) {
        this.CLIENT_ID = clientId;
    }

    private static Thread callbackRunner;

    public synchronized void init() {
        DiscordEventHandlers handlers = new DiscordEventHandlers();

        DiscordRPC.INSTANCE.Discord_Initialize(CLIENT_ID, handlers, true, null);

        if (callbackRunner == null) {
            callbackRunner = new Thread(() -> {
               while (!Thread.currentThread().isInterrupted()) {
                   DiscordRPC.INSTANCE.Discord_RunCallbacks();
                   try {
                       Thread.sleep(100);
                   } catch (InterruptedException innored) {}
               }
            }, "RPC-Callback_Handler");
            callbackRunner.start();
        }
        System.out.println("RPC 클라이언트 실행");
    }

    public void upddatePresence(@Nullable String details) {
        DiscordRichPresence presence = new DiscordRichPresence();
        presence.largeImageKey = "hzicon";
        presence.state = "플레이어: " + Minecraft.getMinecraft().getSession().getUsername();
        if (details != null) {
            presence.details = details;
            presence.startTimestamp = System.currentTimeMillis() / 1000;
        }

        DiscordRPC.INSTANCE.Discord_UpdatePresence(presence);
    }
}
