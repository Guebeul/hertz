package kr.guebeul.hertz.gui;

import kr.guebeul.hertz.gui.inventory.ContainerInv;
import kr.guebeul.hertz.gui.inventory.GuiInv;
import kr.guebeul.hertz.gui.radio.ContainerRadio;
import kr.guebeul.hertz.gui.radio.GuiRadio;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {

    private static int inventoryIndex = 0;
    private static int radioIndex = 1;
    public static final int GUI_CUSTOM_INV = inventoryIndex++;
    public static final int GUI_RADIO = radioIndex++;

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == GUI_CUSTOM_INV)
            return new ContainerInv(player, player.inventory, true);
        else if (ID == GUI_RADIO)
            return new ContainerRadio(player, player.inventory, true);
        else
            return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == GUI_CUSTOM_INV)
            return new GuiInv(player, player.inventory);
        else if (ID == GUI_RADIO)
            return new GuiRadio(player, player.inventory);
        else
            return null;
    }
}
