package kr.guebeul.hertz.gui;

import kr.guebeul.hertz.capabilities.fatigue.FatigueProvider;
import kr.guebeul.hertz.capabilities.fatigue.IFatigue;
import kr.guebeul.hertz.capabilities.temperature.ITemperature;
import kr.guebeul.hertz.capabilities.temperature.TempProvider;
import kr.guebeul.hertz.capabilities.thirst.IThirst;
import kr.guebeul.hertz.capabilities.thirst.ThirstProvider;
import kr.guebeul.hertz.util.Reference;
import kr.guebeul.hertz.util.TextureRender;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.ProgressManager;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.lwjgl.opengl.GL11;

public class HertzHUD extends Gui {

    private Bar bar;

    private final ResourceLocation BAR_OTHER = new ResourceLocation(Reference.MOD_ID + ":textures/gui/overlay/bar_other.png");
    private final ResourceLocation ICON = new ResourceLocation(Reference.MOD_ID + ":textures/gui/overlay/icon.png");
    private final ResourceLocation FATIGUE_ICON = new ResourceLocation(Reference.MOD_ID + ":textures/gui/overlay/a1.png");
    private final ResourceLocation HEALTH_ICON = new ResourceLocation(Reference.MOD_ID + ":textures/gui/overlay/a2.png");


    private final int BAR_WIDTH = 110, BAR_HEIGHT = 13, HEALTH_TEXTURE_HEIGHT = 160, ICON_HEIGHT = 53;

    public HertzHUD() { }

    @SubscribeEvent
    public void renderOverlay(RenderGameOverlayEvent event) {
        if (event.getType().equals(RenderGameOverlayEvent.ElementType.EXPERIENCE) || event.getType().equals(RenderGameOverlayEvent.ElementType.FOOD) || event.getType().equals(RenderGameOverlayEvent.ElementType.HEALTH)) {
            event.setCanceled(true);
            Minecraft mc = Minecraft.getMinecraft();
            EntityPlayer player = mc.player;

            IFatigue fatigue = player.getCapability(FatigueProvider.FATIGUE_CAPABILITY, null);
            IThirst thirst = player.getCapability(ThirstProvider.THIRST_CAPABILITY, null);
            ITemperature temperature = player.getCapability(TempProvider.TEMP_CAP, null);

            float HealthUnit = (float)BAR_WIDTH / player.getMaxHealth();
            int currentHealthWidth = (int)(HealthUnit * player.getHealth());

            float FatigueUnit = (float)BAR_WIDTH / fatigue.getMaxFatigue();
            int currentFatigueUnit = (int)(FatigueUnit * fatigue.getFatigue());

            float TirstUnit = (float)HEALTH_TEXTURE_HEIGHT / thirst.getMaxThirst();
            int currentThirstHeight = (int)(TirstUnit * thirst.getThirst());

            ScaledResolution scaledResolution = new ScaledResolution(mc);
            int posX = scaledResolution.getScaledWidth();
            int posY = scaledResolution.getScaledHeight();

            GL11.glPushMatrix(); {
                mc.renderEngine.bindTexture(BAR_OTHER);
                GL11.glEnable(GL11.GL_BLEND);
                GlStateManager.color(1.0F,1.0F,1.0F,1.0F);
                drawTexturedModalRect(posX / 2 - 280, posY / 2 + 160, 0, 0, BAR_WIDTH, BAR_HEIGHT); // 체력 베이스
                drawTexturedModalRect(posX / 2 - 280, posY / 2 + 160, 0, 14, currentHealthWidth, BAR_HEIGHT); //체력바
                drawTexturedModalRect(posX / 2 - 280, posY / 2 + 140, 0, 0, BAR_WIDTH, BAR_HEIGHT); // 피로도 베이스
                drawTexturedModalRect(posX / 2 - 280, posY / 2 + 140, 0, 14, currentFatigueUnit, BAR_HEIGHT); // 피로도바
            }
            GL11.glPopMatrix();
            TextureRender.drawTextureSimple(HEALTH_ICON, posX / 2 - 313, posY / 2 + 158, 28, 19); //아이콘
            TextureRender.drawTextureSimple(FATIGUE_ICON, posX / 2 - 313, posY / 2 + 138, 28, 19); //아이콘
        }
    }
}
