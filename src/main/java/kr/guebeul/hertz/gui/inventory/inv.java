package kr.guebeul.hertz.gui.inventory;

import com.mojang.realmsclient.gui.ChatFormatting;
import jdk.internal.dynalink.linker.LinkerServices;
import kr.guebeul.hertz.Hertz;
import kr.guebeul.hertz.items.ModItems;
import kr.guebeul.hertz.packet.SyncInventoryPacket;
import kr.guebeul.hertz.packet.SyncPlayerDataPacket;
import kr.guebeul.hertz.util.Reference;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.datafix.walkers.ItemStackData;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import scala.tools.nsc.transform.SpecializeTypes;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.swing.plaf.basic.BasicComboBoxUI;
import java.lang.ref.WeakReference;
import java.util.concurrent.Callable;

public class inv implements IInventory{

    public static int INV_SIZE;
    public static boolean NoNumRadio = false;
    public NonNullList<ItemStack> inventory;

    private final String tagName = "hzinvtags";
    private final String tagSlot = "Slot";

    public WeakReference<EntityPlayer> player;

    @CapabilityInject(IItemHandler.class)
    static Capability<IItemHandler> ITEM_HANDLER_CAPABILITY = null;

    public inv(EntityPlayer player) {
        INV_SIZE = 2 * Reference.HOTBAR_SIZE + Reference.V_SIZE * Reference.getMaxSections();
        inventory = NonNullList.withSize(INV_SIZE, ItemStack.EMPTY);
        this.player = new WeakReference<EntityPlayer>(player);
    }

    @Override
    public int getSizeInventory() {
        return INV_SIZE;
    }

    @Override
    public ItemStack getStackInSlot(int slot) {
        if (slot >= inventory.size()) {
            return ItemStack.EMPTY;
        }
        return inventory.get(slot);
    }

    public void dropStackInSlot(EntityPlayer p, int slot) {
        ItemStack itemstack = getStackInSlot(slot);
        if (!itemstack.isEmpty()) {
            p.dropItem(itemstack, false);
        }
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        ItemStack itemstack = null;
        return itemstack;
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack stack) {
        if (!stack.isEmpty() && stack.getCount() > this.getInventoryStackLimit()) {
            stack.setCount(this.getInventoryStackLimit());
        }
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public void markDirty() {
    }

    @Override
    public boolean isItemValidForSlot(int slot, ItemStack stack) {
        return true;
    }

    public void readFromNBT(NBTTagCompound compound) {
        NBTTagList nbtTagList = compound.getTagList(tagName, Constants.NBT.TAG_COMPOUND);
        ItemStack itemStack;
        for (int i = 0; i < nbtTagList.tagCount(); ++i) {
            NBTTagCompound tags = nbtTagList.getCompoundTagAt(i);
            int b = tags.getInteger(tagSlot);
            itemStack = new ItemStack(tags);
            if (b >= 0 && b < this.getSizeInventory()) {
                this.setInventorySlotContents(b, itemStack);
            }
        }
    }

    public void writeToNBT(NBTTagCompound compound) {
        NBTTagList nbtTagList = new NBTTagList();
        NBTTagCompound tagCompound;
        for (int i = 0; i < this.getSizeInventory(); ++i) {
            if (!this.getStackInSlot(i).isEmpty()) {
                tagCompound = new NBTTagCompound();
                tagCompound .setInteger(tagSlot, i);
                this.getStackInSlot(i).writeToNBT(tagCompound);
                nbtTagList.appendTag(tagCompound);
            }
        }
        compound.setTag(tagName, nbtTagList);
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }

    @Override
    public void openInventory(EntityPlayer player) {
    }

    @Override
    public void closeInventory(EntityPlayer player) {
    }

    @Override
    public int getField(int id) {
        return 0;
    }

    @Override
    public void setField(int id, int value) {
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public void clear() {
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public ItemStack removeStackFromSlot(int slot) {
        // was getStackInSlotOnClosing
        ItemStack stack = getStackInSlot(slot);
        if (stack.isEmpty()) {
            setInventorySlotContents(slot, ItemStack.EMPTY);
        }
        return stack;
    }

    @Override
    public ITextComponent getDisplayName() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer player) {
        return false;
    }

    public void syncSlotToClient(int slot) {
        try {
            if (FMLClientHandler.instance().getClient().world == null) {
                Hertz.instance.network.sendToAll(new SyncInventoryPacket(player.get(), slot));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

