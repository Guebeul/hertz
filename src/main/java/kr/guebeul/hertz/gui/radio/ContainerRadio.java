package kr.guebeul.hertz.gui.radio;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;

public class ContainerRadio extends Container {

    public boolean isLocalWorld;

    public ContainerRadio(EntityPlayer player, InventoryPlayer inventoryPlayer, boolean localWorld) {
        this.isLocalWorld = localWorld;
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }
}
