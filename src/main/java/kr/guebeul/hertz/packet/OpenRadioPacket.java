package kr.guebeul.hertz.packet;

import io.netty.buffer.ByteBuf;
import kr.guebeul.hertz.Hertz;
import kr.guebeul.hertz.gui.GuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class OpenRadioPacket implements IMessage, IMessageHandler<OpenRadioPacket, IMessage> {

    public OpenRadioPacket() {}
    NBTTagCompound tags = new NBTTagCompound();
    public OpenRadioPacket(NBTTagCompound ptags) { tags = ptags; }

    @Override
    public void fromBytes(ByteBuf buf) {
        ByteBufUtils.readTag(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeTag(buf, this.tags);
    }

    @Override
    public IMessage onMessage(OpenRadioPacket message, MessageContext ctx) {
        EntityPlayer p = ctx.getServerHandler().player;
        p.openGui(Hertz.instance, GuiHandler.GUI_RADIO, p.world, (int) p.posX, (int) p.posY, (int) p.posZ);
        return null;
    }
}
