package kr.guebeul.hertz.packet;

import io.netty.buffer.ByteBuf;
import kr.guebeul.hertz.util.PlayerInventoryFile;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class SyncInventoryPacket implements IMessage, IMessageHandler<SyncInventoryPacket, IMessage> {
    int slot;
    int playerId;
    ItemStack itemStack = null;
    public SyncInventoryPacket() {}
    public SyncInventoryPacket(EntityPlayer player, int slot) {
        this.slot = slot;
        this.itemStack = PlayerInventoryFile.getPlayerInventoryStack(player, slot);
        this.playerId = player.getEntityId();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeByte(slot);
        buf.writeInt(playerId);
        ByteBufUtils.writeItemStack(buf, itemStack);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        slot = buf.readByte();
        playerId = buf.readInt();
        itemStack = ByteBufUtils.readItemStack(buf);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IMessage onMessage(SyncInventoryPacket message, MessageContext ctx) {
        Minecraft.getMinecraft().addScheduledTask(new Runnable() {
            @Override
            public void run() {
                processMessage(message);
            }
        });
        return null;
    }

    @SideOnly(Side.CLIENT)
    void processMessage(SyncInventoryPacket message) {
        World world = FMLClientHandler.instance().getClient().world;
        if (world == null)
            return;
        Entity p = world.getEntityByID(message.playerId);
        if (p != null && p instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) p;
            PlayerInventoryFile.setPlayerInventoryStack(player, slot, message.itemStack);
        }
        return;
    }
}
