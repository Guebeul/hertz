package kr.guebeul.hertz.packet;

import io.netty.buffer.ByteBuf;
import kr.guebeul.hertz.Hertz;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IThreadListener;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class SyncPlayerDataPacket implements IMessage, IMessageHandler<SyncPlayerDataPacket, IMessage> {
    public static final int ID = 50;
    NBTTagCompound tags = new NBTTagCompound();
    public SyncPlayerDataPacket() {}

    public SyncPlayerDataPacket(NBTTagCompound ptags) {
        tags = ptags;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        tags = ByteBufUtils.readTag(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeTag(buf, this.tags);
    }

    @Override
    public IMessage onMessage(SyncPlayerDataPacket message, MessageContext ctx) {
        if (ctx.side == Side.CLIENT) {
            SyncPlayerDataPacket.checkThreadAndEnqueue(message, ctx);
        }
        return null;
    }

    private static final void checkThreadAndEnqueue(final SyncPlayerDataPacket message, final MessageContext ctx) {
        IThreadListener thread = ctx.getServerHandler().player.getServer();
        thread.addScheduledTask(new Runnable() {
            @Override
            public void run() {
                Hertz.proxy.setClientPlayerData(ctx, message.tags);
            }
        });
    }
}
