package kr.guebeul.hertz.packet.fatigue;

import com.mojang.realmsclient.gui.ChatFormatting;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class FatiguePacket implements IMessage, IMessageHandler<FatiguePacket, IMessage> {
    @Override
    public void toBytes(ByteBuf buf) { }

    @Override
    public void fromBytes(ByteBuf buf) { }

    @Override
    public IMessage onMessage(FatiguePacket message, MessageContext ctx) {
        String title = "[ " + ChatFormatting.DARK_PURPLE + "헤르츠" + ChatFormatting.WHITE + " ] ";
        EntityPlayer p = ctx.getServerHandler().player;
        p.sendMessage(new TextComponentString(title + "피로도가 " + ChatFormatting.AQUA + "심각하게" + ChatFormatting.WHITE + " 높습니다."));
        p.addPotionEffect(new PotionEffect(MobEffects.SLOWNESS, 99999, 1));
        return null;
    }

    public static class FatiguePacketExit implements IMessage, IMessageHandler<FatiguePacketExit, IMessage> {
        @Override
        public void toBytes(ByteBuf buf) { }

        @Override
        public void fromBytes(ByteBuf buf) { }

        @Override
        public IMessage onMessage(FatiguePacketExit message, MessageContext ctx) {
            String title = "[ " + ChatFormatting.DARK_PURPLE + "헤르츠" + ChatFormatting.WHITE + " ] ";
            EntityPlayer p = ctx.getServerHandler().player;
            p.sendMessage(new TextComponentString(title + "피로가 조금 가신 것 같습니다."));
            p.removePotionEffect(MobEffects.SLOWNESS);
            return null;
        }
    }
}
