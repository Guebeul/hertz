package kr.guebeul.hertz.packet.food;

import com.mojang.realmsclient.gui.ChatFormatting;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class FoodPacket implements IMessage, IMessageHandler<FoodPacket, IMessage> {
    @Override
    public void toBytes(ByteBuf buf) { }

    @Override
    public void fromBytes(ByteBuf buf) { }

    @Override
    public IMessage onMessage(FoodPacket message, MessageContext ctx) {
        String title = "[ " + ChatFormatting.DARK_PURPLE + "헤르츠" + ChatFormatting.WHITE + " ] ";
        EntityPlayer p = ctx.getServerHandler().player;
        p.sendMessage(new TextComponentString(title + "배고픔이 " + ChatFormatting.AQUA + "심각하게" + ChatFormatting.WHITE + " 낮습니다."));
        p.addPotionEffect(new PotionEffect(MobEffects.FIRE_RESISTANCE, 99999, 1));
        return null;
    }

    public static class FoodPacketExit implements IMessage, IMessageHandler<FoodPacketExit, IMessage> {
        @Override
        public void toBytes(ByteBuf buf) { }

        @Override
        public void fromBytes(ByteBuf buf) { }

        @Override
        public IMessage onMessage(FoodPacketExit message, MessageContext ctx) {
            String title = "[ " + ChatFormatting.DARK_PURPLE + "헤르츠" + ChatFormatting.WHITE + " ] ";
            EntityPlayer p = ctx.getServerHandler().player;
            p.sendMessage(new TextComponentString(title + "배고픔이 조금 채워졌습니다."));
            p.removePotionEffect(MobEffects.FIRE_RESISTANCE);
            return null;
        }
    }
}
