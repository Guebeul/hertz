package kr.guebeul.hertz.packet.temp;

import com.mojang.realmsclient.gui.ChatFormatting;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class Temp20Packet implements IMessage, IMessageHandler<Temp20Packet, IMessage> {
    @Override
    public void fromBytes(ByteBuf buf) {

    }

    @Override
    public void toBytes(ByteBuf buf) {

    }

    @Override
    public IMessage onMessage(Temp20Packet message, MessageContext ctx) {
        String title = "[ " + ChatFormatting.DARK_PURPLE + "헤르츠" + ChatFormatting.WHITE + " ] ";
        EntityPlayer p = ctx.getServerHandler().player;
        p.sendMessage(new TextComponentString(title + "체온이 " + ChatFormatting.BLUE + "심각하게" + ChatFormatting.WHITE + "낮습니다!"));
        p.removePotionEffect(MobEffects.UNLUCK);
        p.removePotionEffect(MobEffects.LUCK);
        p.addPotionEffect(new PotionEffect(MobEffects.WEAKNESS, 99999, 1));
        return null;
    }
}
