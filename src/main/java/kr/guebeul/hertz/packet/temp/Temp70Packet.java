package kr.guebeul.hertz.packet.temp;

import com.mojang.realmsclient.gui.ChatFormatting;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class Temp70Packet implements IMessage, IMessageHandler<Temp70Packet, IMessage> {
    @Override
    public void fromBytes(ByteBuf buf) {

    }

    @Override
    public void toBytes(ByteBuf buf) {

    }

    @Override
    public IMessage onMessage(Temp70Packet message, MessageContext ctx) {
        String title = "[ " + ChatFormatting.DARK_PURPLE + "헤르츠" + ChatFormatting.WHITE + " ] ";
        EntityPlayer p = ctx.getServerHandler().player;
        p.sendMessage(new TextComponentString(title + "체온이 낮습니다!"));
        p.removePotionEffect(MobEffects.LUCK);
        p.removePotionEffect(MobEffects.WEAKNESS);
        p.addPotionEffect(new PotionEffect(MobEffects.UNLUCK, 99999, 1));
        return null;
    }


    public static class Temp70PacketExit implements IMessage, IMessageHandler<Temp70PacketExit, IMessage> {
        @Override
        public void fromBytes(ByteBuf buf) {

        }

        @Override
        public void toBytes(ByteBuf buf) {

        }

        @Override
        public IMessage onMessage(Temp70PacketExit message, MessageContext ctx) {
            String title = "[ " + ChatFormatting.DARK_PURPLE + "헤르츠" + ChatFormatting.WHITE + " ] ";
            EntityPlayer p = ctx.getServerHandler().player;
            p.sendMessage(new TextComponentString(title + "체온이 정상적으로 돌아왔습니다."));
            p.removePotionEffect(MobEffects.UNLUCK);
            return null;
        }
    }
}
