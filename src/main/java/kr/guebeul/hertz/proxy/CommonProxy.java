package kr.guebeul.hertz.proxy;

import kr.guebeul.hertz.Hertz;
import kr.guebeul.hertz.capabilities.fatigue.Fatigue;
import kr.guebeul.hertz.capabilities.fatigue.FatigueStorge;
import kr.guebeul.hertz.capabilities.fatigue.IFatigue;
import kr.guebeul.hertz.capabilities.temperature.ITemperature;
import kr.guebeul.hertz.capabilities.temperature.TempStorage;
import kr.guebeul.hertz.capabilities.temperature.Temperature;
import kr.guebeul.hertz.capabilities.thirst.IThirst;
import kr.guebeul.hertz.capabilities.thirst.Thirst;
import kr.guebeul.hertz.capabilities.thirst.ThirstStorage;
import kr.guebeul.hertz.items.ModItems;
import kr.guebeul.hertz.util.handler.CapabilityHandler;
import kr.guebeul.hertz.util.handler.EventHandler;
import kr.guebeul.hertz.util.handler.SoundsHandler;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IThreadListener;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CommonProxy {

    public void registerHandlers() {

        MinecraftForge.EVENT_BUS.register(new CapabilityHandler());

        CapabilityManager.INSTANCE.register(IFatigue.class, new FatigueStorge(), Fatigue::new);
        CapabilityManager.INSTANCE.register(ITemperature.class, new TempStorage(), Temperature::new);
        CapabilityManager.INSTANCE.register(IThirst.class, new ThirstStorage(), Thirst::new);

        ModItems.init();
        SoundsHandler.init();

        registerRendering();
        registerAnimations();
    }

    public IThreadListener getThreadFromContext(MessageContext ctx) {
        return ctx.getServerHandler().player.getServer();
    }

    public void registerRendering() {}
    public void registerAnimations() {}
    public void setClientPlayerData(MessageContext ctx, NBTTagCompound tags) {}

    public void rpcinit(Hertz main) {}
    public void rpcupdate(Hertz main, String details) {}
}
