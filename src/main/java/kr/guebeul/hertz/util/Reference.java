package kr.guebeul.hertz.util;

public class Reference {
    public static final String MOD_ID = "hertz";
    public static final String MC_VERSION = "1.12.2";
    public static final String NAME = "Hertz";
    public static final String COMMON_PROXY = "kr.guebeul.hertz.proxy.CommonProxy";
    public static final String CLIENT_PROXY = "kr.guebeul.hertz.proxy.ClientProxy";

    public static final int ENTITY_BEAR = 120;
    public static final int ENTITY_TEST = 121;

    public static final int SQ = 18;
    public static final int HOTBAR_SIZE = 9;
    public static final int ARMOR_SIZE = 4;
    public static final int ROWS_VANILLA = 3;
    public static final int COLS_VANILLA = 9;
    public static final int V_SIZE = ROWS_VANILLA * COLS_VANILLA;
    public static final int VWIDTH = 176;
    public static final int VHEIGHT = 166;

    public static boolean showGuiButton = true;
    public static final String categoryHighlander = "Hertz";
    private static int TEXTURE_WIDTH = 640;
    private static int TEXTURE_HEIGHT = 360;
    private static boolean isLarge = false;
    public static int getInvWidth() {
        return TEXTURE_WIDTH;
    }
    public static int getInvHeight() {
        return TEXTURE_HEIGHT;
    }
    public static int getMaxSections() {
        return (isLarge) ? 15 : 6;
    }

    public static boolean enableTemperature = true;
    public static boolean enableWetness = true;
    public static boolean showCelsius = false;
    public static boolean showSipsInDurabilityBar = false;
    public static final String barPositions = "moddle right";
    public static double wetnessScale = 1.0;
    public static double temperatureScale = 1.0;

}
