package kr.guebeul.hertz.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiLabel;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;

public class TextRender {

    @SideOnly(Side.CLIENT)
    public static void drawStringSimple(String text, int x, int y, int color, boolean shadow) {
        Minecraft.getMinecraft().fontRenderer.drawString(text, x, y, color, shadow);
    }
}
