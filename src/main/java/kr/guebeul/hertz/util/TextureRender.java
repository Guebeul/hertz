package kr.guebeul.hertz.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;

import javax.swing.*;

public class TextureRender {

    @SideOnly(Side.CLIENT)
    public static void drawTextureSimple(ResourceLocation res, int x, int y, int w, int h) {
        GL11.glPushMatrix(); {
            Minecraft.getMinecraft().getTextureManager().bindTexture(res);
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            Gui.drawModalRectWithCustomSizedTexture(x, y, 0F, 0F, w, h, w, h);
        }
        GL11.glPopMatrix();
    }

    @SideOnly(Side.CLIENT)
    public static void drawTextureSimpleWithUV(ResourceLocation res, int x, int y, int U, int V , int width , int height) {
        GL11.glPushMatrix(); {
            Minecraft.getMinecraft().getTextureManager().bindTexture(res);
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glColor4f(1.0F,1.0F,1.0F,1.0F);
            Gui.drawModalRectWithCustomSizedTexture(x, y, U, V, width, height, width, height);
        }
        GL11.glPopMatrix();
    }
}
