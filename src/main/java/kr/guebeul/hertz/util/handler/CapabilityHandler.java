package kr.guebeul.hertz.util.handler;

import kr.guebeul.hertz.capabilities.fatigue.FatigueProvider;
import kr.guebeul.hertz.capabilities.temperature.TempProvider;
import kr.guebeul.hertz.capabilities.thirst.ThirstProvider;
import kr.guebeul.hertz.gui.inventory.inv;
import kr.guebeul.hertz.util.Reference;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class CapabilityHandler {

    public static final ResourceLocation FATIGUE_CAP = new ResourceLocation(Reference.MOD_ID, "fatigue");
    public static final ResourceLocation TEMP_CAP = new ResourceLocation(Reference.MOD_ID, "temperature");
    public static final ResourceLocation THIRST_CAP = new ResourceLocation(Reference.MOD_ID, "thirst");

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void attachCapability(AttachCapabilitiesEvent<Entity> event) {
        if (!(event.getObject() instanceof EntityPlayer)) return;

        event.addCapability(FATIGUE_CAP, new FatigueProvider());
        event.addCapability(TEMP_CAP, new TempProvider());
        event.addCapability(THIRST_CAP, new ThirstProvider());
    }
}
