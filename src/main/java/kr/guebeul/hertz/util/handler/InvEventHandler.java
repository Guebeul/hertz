package kr.guebeul.hertz.util.handler;

import kr.guebeul.hertz.util.PlayerInventoryFile;
import kr.guebeul.hertz.gui.inventory.inv;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber
public class InvEventHandler {

    public inv inv;

    @SubscribeEvent
    public void playerLoggedInEvent(net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent event) {
        Side side = FMLCommonHandler.instance().getEffectiveSide();
        if (side == Side.SERVER) {
            PlayerInventoryFile.playerEntityIds.add(event.player.getEntityId());
        }
    }

    @SubscribeEvent
    public void playerTick(LivingEvent.LivingUpdateEvent event) {
        if (event.getEntity() instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) event.getEntity();
            if (!PlayerInventoryFile.playerEntityIds.isEmpty() && PlayerInventoryFile.playerEntityIds.contains(player.getEntityId())) {
                PlayerInventoryFile.syncItems(player);
                PlayerInventoryFile.playerEntityIds.remove(player.getEntityId());
            }
        }
    }


    @SubscribeEvent
    public void playerLoad(PlayerEvent.LoadFromFile event) {
        PlayerInventoryFile.playerSetupOnLoad(event);
    }

    @SubscribeEvent
    public void playerSave(PlayerEvent.SaveToFile event) {
        PlayerInventoryFile.savePlayerItems(event.getEntityPlayer(), PlayerInventoryFile.getPlayerFile(PlayerInventoryFile.EXT, event.getPlayerDirectory(), event.getEntityPlayer()), PlayerInventoryFile.getPlayerFile(PlayerInventoryFile.EXTBK, event.getPlayerDirectory(), event.getEntityPlayer()));
    }
}
