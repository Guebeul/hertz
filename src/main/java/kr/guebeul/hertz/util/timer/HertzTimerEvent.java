package kr.guebeul.hertz.util.timer;

public abstract class HertzTimerEvent {

    public abstract void run();
}
